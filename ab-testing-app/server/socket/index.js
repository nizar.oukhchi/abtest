const socketIo = require("socket.io");

module.exports = function(server, app, agenda) {
    const io = socketIo(server);
    app.io = io;
    agenda.io = io;
    io.on("connection", socket => {
        console.log("Agent connected");

        socket.on("subscribe", room => {
            console.log("Agent is asking to subscribe to: ", room);
            socket.join(room);
        });
    });
};
