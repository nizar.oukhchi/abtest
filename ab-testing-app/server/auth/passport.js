const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const { ExtractJwt } = require("passport-jwt");
const LocalStrategy = require("passport-local");

const Client = require("../api/client/client.model");
const SECRET = process.env.SECRET;

const localOptions = { usernameField: "email" };

const localLogin = new LocalStrategy(
  localOptions,
  async (email, password, done) => {
    try {
      let client = await Client.findOne({ email: email })
        .populate("account")
        .populate("accounts");
      if (!client) {
        return done(null, false);
      }

      client.comparePasswords(password, async (err, isMatch) => {
        if (err) {
          return done(err);
        }

        if (!isMatch) {
          return done(null, false);
        }

        if (client.accounts && client.accounts.length > 0) {
          return done(null, client);
        } else {
          client.accounts = [client.account];
          client = await client.save();
          return done(null, client);
        }
      });
    } catch (err) {
      console.log(err);
      return done(err);
    }
  }
);

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET,
};

const jwtLogin = new JwtStrategy(jwtOptions, async (payload, done) => {
  try {
    const client = await Client.findById(payload.sub)
      .populate("account")
      .populate("accounts");
    if (client) {
      done(null, client);
    } else {
      done(null, false);
    }
  } catch (err) {
    console.log(err);
    done(err, false);
  }
});

passport.use(localLogin);
passport.use(jwtLogin);
