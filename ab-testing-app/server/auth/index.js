const passport = require("passport");
const jwt = require("jwt-simple");

const Client = require("../api/client/client.model");
const Account = require("../api/client/account.model");
const requireSignIn = require("./requireSignIn");
const SECRET = process.env.SECRET;
require("./passport");

function tokenForUser(client) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: client.id, iat: timestamp }, SECRET);
}

module.exports = (server) => {
  server.post(
    "/api/auth/login",
    passport.authenticate("local", { session: false }),
    function (req, res) {
      res.send({ user: req.user, token: tokenForUser(req.user) });
    }
  );

  server.post("/api/auth/signup", async (req, res, next) => {
    const { email, password, name, accountName } = req.body;

    if (!email || !password) {
      return res
        .status(422)
        .send({ error: "Email and password must be provided" });
    }

    try {
      const existingUser = await Client.findOne({ email: email });

      if (existingUser) {
        return res.status(422).send({ error: "Email is already in use..." });
      }

      const account = await Account.create({ name: accountName });

      let client = new Client({
        email,
        password,
        name,
        account,
        accounts: [account],
      });

      client.encryptPassword(async () => {
        client = await client.save();
        return res.json({ user: client, token: tokenForUser(client) });
      });
    } catch (err) {
      return next(err);
    }
  });

  server.post("/api/auth/google", async (req, res, next) => {
    const { email, googleId, name } = req.body;

    if (!email || !googleId) {
      return res
        .status(422)
        .send({ error: "Email and googleId must be provided" });
    }

    try {
      let existingUser = await Client.findOne({ email: email })
        .populate("account")
        .populate("accounts");
      if (existingUser) {
        if (existingUser.googleId) {
          if (existingUser.accounts && existingUser.accounts.length > 0) {
            return res.json({
              user: existingUser,
              token: tokenForUser(existingUser),
            });
          } else {
            existingUser.accounts = [existingUser.account];
            existingUser = await existingUser.save();
            return res.json({
              user: existingUser,
              token: tokenForUser(existingUser),
            });
          }
        } else {
          existingUser.googleId = googleId;
          existingUser = await existingUser.save();
          return res.json({
            user: existingUser,
            token: tokenForUser(existingUser),
          });
        }
      } else {
        let account = new Account();
        account = await account.save();
        let client = new Client({
          email,
          googleId,
          name,
          account: account,
          accounts: [account],
        });
        client = await client.save();
        return res.json({
          user: client,
          token: tokenForUser(client),
        });
      }
    } catch (err) {
      console.log(err);
      return next(err);
    }
  });

  server.get("/api/auth/me", requireSignIn, (req, res, next) =>
    res.status(200).json({ user: req.user })
  );
};
