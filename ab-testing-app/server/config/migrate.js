const Account = require("../api/client/account.model");
const Client = require("../api/client/client.model");

const migrate = async () => {
  try {
    const accounts = await Account.find();
    accounts.forEach(async (item) => {
      item.new = false;
      item.save();
    });
    const clients = await Client.find();
    clients.forEach(async (item) => {
      if (item.accounts && item.accounts.length === 0) {
        item.accounts = [item.account];
        await item.save();
      }

      if (item.role === "master") {
        item.accounts = accounts.map((element) => element._id);
        await item.save();
      }
    });
  } catch (err) {
    console.log(err);
  }
};

module.exports = migrate;
