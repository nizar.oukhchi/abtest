const Agenda = require("agenda");

const agenda = new Agenda({ db: { address: process.env.MONGODB_URI } });

require("../jobs/thompsonSampling")(agenda);

agenda.start();

module.exports = agenda;
