const url = require('url');

const cleanUrl = (urlToClean) => {
  let parsedUrl = url.parse(urlToClean)
  let preCleanUrl = `${parsedUrl.host}${parsedUrl.pathname}`.replace(/^(?:https?:\/\/)?(?:www\.)?(?:nullwww\.)?(?:null)?/i, "");
  const lastIndex = preCleanUrl.length - 1;
  return preCleanUrl[lastIndex] === "/" ? preCleanUrl.substring(0, lastIndex) : preCleanUrl;
};

module.exports = cleanUrl;
