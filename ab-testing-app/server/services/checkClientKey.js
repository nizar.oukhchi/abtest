const boom = require("boom");
const Account = require("../api/client/account.model");

module.exports = async (req, res, next) => {
  const clientKey = req.get("Client-Key");
  if (/CLIENT-KEY-\d{5}/i.test(clientKey)) {
    try {
      const account = await Account.findOne({ clientKey });

      if (account) {
        req.account = account;
        next();
      } else {
        next(boom.badRequest("Invalid Client key"));
      }
    } catch (err) {
      next(boom.badImplementation("An internal server error occurred"));
    }
  } else {
    return next(
      boom.unauthorized("Client Key is mandatory to access this service")
    );
  }
};
