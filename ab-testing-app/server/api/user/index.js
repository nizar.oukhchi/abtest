const mongooseCrudify = require("mongoose-crudify");
const boom = require("boom");
const { query, header, validationResult } = require("express-validator");

const helpers = require("../../services/helpers");
const checkClientKey = require("../../services/checkClientKey");
const ThompsonSampling = require("../thompsonSampling/thompsonSampling.model");
const cleanUrl = require("../../utils/cleanUrl");

const User = require("./user.model");

module.exports = function (server) {
  server.get(
    "/api/users/sync",
    checkClientKey,
    [query(["url"]).isString()],
    async (req, res, next) => {
      const userId = req.get("User-id");
      const account = req.account;
      const webSiteUrl = req.query.url && cleanUrl(req.query.url);
      const ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;

      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const errorsMessages = errors
          .array()
          .map(
            ({ param, location, msg }) => `${param} in ${location} : ${msg}`
          );
        return next(boom.badRequest(errorsMessages));
      }

      try {
        if (userId && userId !== undefined) {
          const user = await User.findById(userId);
          const thompson = await ThompsonSampling.findById(
            user.thompson
          ).populate("variants.domElements");

          const variant = thompson.variants
            .find((i) => i.id === user.variant)
            .toUser();

          const conversionSelectors = variant.generateConversionSelector(
            thompson.conversionButtonSelector
          );

          const data = {
            id: user.id,
            conversionSelectors,
            variant,
            conversionPixels: thompson.conversionPixels,
          };

          return res.status(200).json(data);
        } else {
          const thompson = await ThompsonSampling.findOne({
            creator: account.id,
            url: new RegExp(webSiteUrl + "$", "i"),
            status: "LIVE",
          }).populate("variants.domElements");

          if (thompson) {
            const variant = thompson.chooseVariant();
            thompson.incrementVariantVisits(variant._id);
            const user = await User.create({
              ip,
              thompson,
              variant: variant._id,
            });
            const conversionSelectors = variant.generateConversionSelector(
              thompson.conversionButtonSelector
            );
            const data = {
              id: user.id,
              conversionSelectors,
              variant: variant.toUser(),
              conversionPixels: thompson.conversionPixels,
            };
            return res.status(200).json(data);
          } else
            return res.status(404).json({
              error: "There is no algorithm associated with this client or url",
            });
        }
      } catch (err) {
        console.log(err);
      }
    }
  );

  server.get("/api/users/converted", async (req, res) => {
    const userId = req.get("User-Id");
    let variantId;
    if (userId && userId !== "undefined") {
      try {
        const user = await User.findById(userId);
        variantId = user.variant;

        const thompson = await ThompsonSampling.findById(user.thompson);
        if (!user.converted) {
          thompson.incrementVariantConversion(variantId);
          user.converted = true;
          await user.save();
        }

        return res.status(200).json({ message: "converted" });
      } catch (err) {
        console.log(err);
      }
    } else {
      return res.status(404);
    }
  });

  server.use(
    "/api/users",
    mongooseCrudify({
      Model: User,
      selectFields: "-__v",
      endResponseInAction: false,
      actions: {
        // default actions: list, create, read, update, delete
        // any non-overridden action will be in functional
        // store query result or err in req.crudify
        // auto calling next() if after actions defined for this action

        // override read
        list: (req, res) => {
          const { algorithm } = req.query;
          User.find({ algorithm })
            .select("-__v")
            .then((usersList) => {
              res.status(200).json(usersList);
            });
        },
      },
      afterActions: [{ middlewares: [helpers.formatResponse] }],
    })
  );
};
