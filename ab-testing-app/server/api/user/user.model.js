const mongoose = require("mongoose");

const { Schema } = mongoose;

const userSchema = new Schema(
    {
        ip: String,
        thompson: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "ThompsonSampling"
        },
        variant: String,
        converted: { type: Boolean, default: false },
        connected: Boolean,
        owner: { type: mongoose.Schema.Types.ObjectId, ref: "Client" }
    },
    { timestamps: true }
);

userSchema.methods.toJSON = function() {
    let obj = this.toObject();
    obj.id = obj._id;
    delete obj._id;
    delete obj.__v;
    return obj;
};

const User = mongoose.model("User", userSchema);

module.exports = User;
