const boom = require("boom");
const { check, param, validationResult } = require("express-validator");

const ThompsonSampling = require("./thompsonSampling.model");
const DomElementStyle = require("../domElementStyle/domElementStyle.model");
const requireSignIn = require("../../auth/requireSignIn");
const agenda = require("../../config/agenda");
const checkClientKey = require("../../services/checkClientKey");

module.exports = function (server) {
  server.get(
    "/api/thompsons/generate/:id",
    requireSignIn,
    async (req, res, next) => {
      const { id } = req.params;

      try {
        let thompsonSampling = await ThompsonSampling.findOneAndUpdate(
          { _id: id },
          { processing: true },
          { new: true }
        ).populate("domElementsCount");

        agenda.now("generate thompsonSampling variants", {
          thompsonSamplingId: id,
          clientId: req.user.id,
        });
        return res.status(200).json(thompsonSampling);
      } catch (e) {
        next(boom.badRequest(e));
      }
    }
  );

  server.get("/api/thompsons", requireSignIn, async (req, res, next) => {
    const { user } = req;

    try {
      const thompsonSamplings = await ThompsonSampling.find({creator: user.account})
        .populate("creator")
        .sort("-updatedAt");
      return res.status(201).json(thompsonSamplings);
    } catch (e) {
      next(boom.badRequest(e));
    }
  });

  server.get("/api/thompsons/:id", requireSignIn, async (req, res, next) => {
    const { id } = req.params;

    try {
      const thompsonSampling = await ThompsonSampling.findById(id)
        .populate("creator")
        .populate("domElementsCount");
      return res.status(201).json(thompsonSampling);
    } catch (e) {
      next(boom.badRequest(e));
    }
  });

  server.post("/api/thompsons", requireSignIn, async (req, res, next) => {
    const { user } = req;
    const { url } = req.body;

    try {
      const thompsonSampling = await ThompsonSampling.create({
        creator: user.account,
        url,
      });
      return res.status(201).json(thompsonSampling);
    } catch (e) {
      next(boom.badRequest(e));
    }
  });

  server.put("/api/thompsons/:id", requireSignIn, async (req, res, next) => {
    const {
      id,
      name,
      url,
      description,
      conversionButtonSelector,
      status,
      conversionPixels
    } = req.body;

    try {
      let thompsonSampling = await ThompsonSampling.findById(id);

      thompsonSampling.name = name || thompsonSampling.name;
      thompsonSampling.status = status || thompsonSampling.status;
      thompsonSampling.url = url || thompsonSampling.url;
      thompsonSampling.description =
        description || thompsonSampling.description;
      thompsonSampling.conversionButtonSelector =
        conversionButtonSelector || thompsonSampling.conversionButtonSelector;
      thompsonSampling.conversionPixels = conversionPixels || thompsonSampling.conversionPixels;

      thompsonSampling = await thompsonSampling.save();
      return res.status(201).json(thompsonSampling);
    } catch (e) {
      next(boom.badRequest(e));
    }
  });

  server.delete("/api/thompsons/:id", requireSignIn, async (req, res, next) => {
    const { id } = req.params;

    try {
      await ThompsonSampling.deleteOne({
        _id: id,
      });
      await DomElementStyle.deleteMany({ thompson: id });
      return res.status(201).json({ id });
    } catch (e) {
      next(boom.badRequest(e));
    }
  });

  server.get(
    "/api/thompsons/preview/:id",
    [param(["id"]).isString()],
    async (req, res, next) => {
      const { id } = req.params;

      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const errorsMessages = errors
          .array()
          .map(
            ({ param, location, msg }) => `${param} in ${location} : ${msg}`
          );
        return next(boom.badRequest(errorsMessages));
      }

      try {
        const { variants } = await ThompsonSampling.findOne({
          variants: {
            $elemMatch: {
              _id: id,
            },
          },
        })
          .populate("variants.domElements")
          .select("variants");

        const previewVariant = variants.find((item) => item.id === id);
        const { domElements } = previewVariant;
        return res.status(201).json({ domElements });
      } catch (e) {
        next(boom.badRequest(e));
      }
    }
  );

  server.put(
    "/api/thompsons/variant/:id",
    requireSignIn,
    [param(["id"]).isString(), check("name").exists().isString()],
    async (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const errorsMessages = errors
          .array()
          .map(
            ({ param, location, msg }) => `${param} in ${location} : ${msg}`
          );
        return next(boom.badRequest(errorsMessages));
      }

      const { id } = req.params;
      const { name } = req.body;

      try {
        const thompson = await ThompsonSampling.findOne({
          variants: {
            $elemMatch: {
              _id: id,
            },
          },
        });
        const variant = thompson.variants.id(id);
        variant.name = name;
        await thompson.save();
        return res.status(201).json(thompson);
      } catch (e) {
        next(boom.badRequest(e));
      }
    }
  );
};
