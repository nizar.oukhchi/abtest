const mongoose = require("mongoose");
const { rbeta, maxIndex } = require("../../utils/math2");
const cleanUrl = require("../../utils/cleanUrl");

const thompsonVariantSchema = new mongoose.Schema(
  {
    text: String,
    name: String,
    visits: { type: Number, default: 0 },
    conversion: { type: Number, default: 0 },
    domElements: [
      { type: mongoose.Schema.Types.ObjectId, ref: "DomElementStyle" },
    ]
  },
  {
    timestamps: true,
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
  }
);

thompsonVariantSchema.virtual("conversionRate").get(function () {
  return ((this.conversion / this.visits) * 100).toFixed(2);
});

thompsonVariantSchema.methods.generateConversionSelector = function (
  conversionButtonSelector = ""
) {
  const clickEventElementSelector = this.domElements.reduce(
    (accumulator, item) => {
      if (item.nodeType === "button" || item.nodeType === "a") {
        return accumulator
          ? `${accumulator}, ${item.selector}`
          : `${item.selector}`;
      } else {
        return accumulator;
      }
    },
    `${conversionButtonSelector}`
  );

  const submitEventElementSelector = this.domElements.reduce(
    (accumulator, item) => {
      if (item.nodeType === "form") {
        return `${accumulator}, ${item.selector}`;
      } else {
        return accumulator;
      }
    },
    ""
  );

  return {
    clickEventElementSelector,
    submitEventElementSelector,
  };
};

thompsonVariantSchema.methods.toUser = function () {
  let obj = this.toObject();
  obj.domElements = obj.domElements.map((i) => ({
    isMultiple: i.isMultiple,
    selector: i.selector,
    content: i.content,
    src: i.src,
    nodeType: i.nodeType,
    style: i.style,
  }));
  delete obj.conversionRate;
  delete obj.name;
  delete obj.visits;
  delete obj.conversion;
  delete obj._id;
  delete obj.__v;
  return obj;
};

const thompsonSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      default: "Unnamed testing",
    },
    description: {
      type: String,
    },
    url: {
      type: String,
      default: "www.test.com",
      required: [true, "web site URL is required"],
    },
    status: {
      type: String,
      enum: ["DRAFT", "LIVE", "SUSPENDED"],
      default: "DRAFT",
    },
    creator: { type: mongoose.Schema.Types.ObjectId, ref: "Account" },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: "Client" },
    variantsNumber: Number,
    variants: [thompsonVariantSchema],
    conversionButtonSelector: String,
    processing: {
      type: Boolean,
      default: false,
    },
    variantsGenerated: {
      type: Boolean,
      default: false,
    },
    conversionPixels: [
      {
        content: String,
        test: {
          type: String,
          enum: ["equals", "contains"],
        },
      },
    ],
  },
  {
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
    timestamps: true,
  }
);

thompsonSchema.virtual("domElementsCount", {
  ref: "DomElementStyle",
  localField: "_id",
  foreignField: "thompson",
  count: true,
});

thompsonSchema.pre("save", function (next) {
  this.url = cleanUrl(this.url);
  next();
});

thompsonSchema.methods.incrementVariantVisits = function (variantId) {
  const variantIndex = this.variants.findIndex((el) => el._id === variantId);
  const variant = this.variants[variantIndex];
  variant.visits++;
  this.variants[variantIndex] = variant;
  return this.save();
};

thompsonSchema.methods.incrementVariantConversion = function (variantId) {
  const variantIndex = this.variants.findIndex(
    (el) => el.id.toString() === variantId
  );

  const variant = this.variants[variantIndex];
  variant.conversion++;
  this.variants[variantIndex] = variant;
  return this.save();
};

thompsonSchema.methods.chooseVariant = function () {
  const a = 1;
  const b = 1;
  const variants = this.variants;
  const probabilities = Array(this.variants.length)
    .fill(0)
    .map((_, i) => {
      const count = variants[i].visits; // Trials
      const successes = variants[i].conversion; // Rewards
      const failures = count - successes;
      return rbeta(a + successes, b + failures);
    });
  return variants[maxIndex(probabilities)];
};

thompsonSchema.methods.toJSON = function () {
  let obj = this.toObject();
  delete obj._id;
  delete obj.__v;
  return obj;
};

const ThompsonSchema = mongoose.model("ThompsonSampling", thompsonSchema);

module.exports = ThompsonSchema;
