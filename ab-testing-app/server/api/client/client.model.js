const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const { Schema } = mongoose;

const clientSchema = new Schema(
  {
    email: {
      type: String,
      unique: true,
      lowercase: true,
    },
    password: String,
    name: String,
    googleId: String,
    accounts: [{ type: mongoose.Schema.Types.ObjectId, ref: "Account" }],
    account: {type: mongoose.Schema.Types.ObjectId, ref: "Account"},
    role: { type: String, default: "user" },
    inviteAccount: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Account",
      default: null,
    },
  },
  {
    timestamps: true,
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
  }
);

clientSchema.virtual("isPasswordDefined").get(function () {
  return this.password && this.password.length > 0;
});

clientSchema.methods.encryptPassword = function (callback) {
  const client = this;
  // generating hashed password
  bcrypt.genSalt(10, function (err, salt) {
    if (err) {
      return callback(err);
    }
    bcrypt.hash(client.password, salt, function (err, hash) {
      if (err) {
        return callback(err);
      }

      client.password = hash;
      callback(null);
    });
  });
};

clientSchema.methods.comparePasswords = function (password, callback) {
  if(this.password){
    bcrypt.compare(password, this.password, function (err, isMatch) {
      if (err) {
        return callback(err);
      }
      callback(null, isMatch);
    });
  }else{
    callback(null, false);
  }
};

clientSchema.methods.toJSON = function () {
  let obj = this.toObject();
  delete obj.password;
  delete obj.__v;
  return obj;
};

const Client = mongoose.model("Client", clientSchema);

module.exports = Client;
