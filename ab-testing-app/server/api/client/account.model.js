const mongoose = require("mongoose");
const generateUniqueKey = require("mongoose-generate-unique-key");
const chance = new require("chance")();

const Client = require("./client.model");

const accountSchema = new mongoose.Schema(
  {
    clientKey: {
      type: String,
      unique: true,
    },
    name: {
      type: String,
      default: () =>
        `account ${chance.string({
          length: 5,
          alpha: true,
          casing: "lower",
          symbols: false,
        })}`,
    },
    new:{
      type: Boolean,
      default: true
    }
  },
  { timestamps: true }
);

accountSchema.plugin(
  generateUniqueKey(
    "clientKey",
    () => `CLIENT-KEY-${String(Math.floor(Math.random() * 1000000) + 10000)}`
  )
);

accountSchema.post("save", async function (doc) {
  if(doc.new){
    try{
      const admins = await Client.find({ role: "master" });
      admins.forEach(async admin => {
        admin.accounts.push(doc)
        await admin.save();
        doc.new = false;
        await doc.save()
      });
    }catch(err){
      console.log(err)
    }
  }
});

const Account = mongoose.model("Account", accountSchema);

module.exports = Account;
