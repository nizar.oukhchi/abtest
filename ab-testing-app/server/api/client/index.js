const mongooseCrudify = require("mongoose-crudify");
const passport = require("passport");
const boom = require("boom");
const { check, param, validationResult } = require("express-validator");

const requireAuth = passport.authenticate("jwt", { session: false });
const helpers = require("../../services/helpers");
const requireSignIn = require("../../auth/requireSignIn");

const Client = require("./client.model");
const Account = require("./account.model");

module.exports = function (server) {
  server.post(
    "/api/clients/:id/update-password",
    requireSignIn,
    [
      param(["id"]).exists({ checkFalsy: true }),
      check("newPassword").exists({ checkFalsy: true }),
    ],
    async (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const errorsMessages = errors
          .array()
          .map(
            ({ param, location, msg }) => `${param} in ${location} : ${msg}`
          );
        return next(boom.badRequest(errorsMessages));
      }
      const { id } = req.params;
      const { oldPassword, newPassword } = req.body;
      try {
        let client = await Client.findById(id)
          .populate("accounts")
          .populated("account");

        if (client.password) {
          client.comparePasswords(oldPassword, async (err, isMatch) => {
            if (err) return next(boom.badRequest(err));
            if (!isMatch)
              return next(boom.badRequest(new Error("Your password is wrong")));

            client.password = newPassword;

            client.encryptPassword(async (err) => {
              if (err) return next(boom.badRequest(err));
              client = await client.save();
              return res.status(201).json(client);
            });
          });
        } else {
          client.password = newPassword;

          client.encryptPassword(async (err) => {
            if (err) return next(boom.badRequest(err));
            client = await client.save();
            return res.status(201).json(client);
          });
        }
      } catch (e) {
        next(boom.badRequest(e));
      }
    }
  );

  server.post(
    "/api/clients/invite",
    requireSignIn,
    [check("email").exists({ checkFalsy: true })],
    async (req, res, next) => {
      const { user } = req;
      const { email } = req.body;

      try {
        const completeUser = await Client.findById(user.id);
        let clientToInvite = await Client.findOne({ email });
        clientToInvite.inviteAccount = completeUser.account;
        clientToInvite = await clientToInvite.save();
        return res.status(201).json(clientToInvite);
      } catch (err) {
        next(boom.badRequest(err));
      }
    }
  );

  server.get(
    "/api/clients/accept-invite",
    requireSignIn,
    async (req, res, next) => {
      const { user } = req;

      try {
        if (user.inviteAccount) {
          let client = await Client.findById(user.id);
          client.accounts.push(client.inviteAccount);
          client.inviteAccount = null;
          client = await client.save();
          client = await Client.findById(user.id)
            .populate("account")
            .populate("accounts");

          return res.status(201).json(client);
        }
        return res.status(201).json(user);
      } catch (err) {
        next(boom.badRequest(err));
      }
    }
  );

  server.get(
    "/api/clients/refuse-invite",
    requireSignIn,
    async (req, res, next) => {
      const { user } = req;

      try {
        if (user.inviteAccount) {
          let client = await Client.findById(user.id).populate("accounts");
          client.inviteAccount = null;
          client = await client.save();

          return res.status(201).json(client);
        }
        return res.status(201).json(user);
      } catch (err) {
        next(boom.badRequest(err));
      }
    }
  );

  server.put(
    "/api/clients/set-account",
    [check("account").exists({ checkFalsy: true })],
    requireSignIn,
    async (req, res, next) => {
      const { user } = req;
      const { account } = req.body;

      try {
        let client = await Client.findById(user.id).populate("account");
        client.account = account;
        client = await client.save();
        client = await Client.findById(user.id)
          .populate("account")
          .populate("accounts");

        return res.status(201).json(client);
      } catch (err) {
        next(boom.badRequest(err));
      }
    }
  );

  server.put(
    "/api/accounts/:id",
    [check("name").exists({ checkFalsy: true })],
    requireSignIn,
    async (req, res, next) => {
      const { user } = req;
      const { id } = req.params;
      const { name } = req.body;

      try {
        let account = await Account.findById(id);
        account.name = name;
        account = await account.save();
        client = await Client.findById(user.id)
          .populate("account")
          .populate("accounts");

        return res.status(201).json(client);
      } catch (err) {
        next(boom.badRequest(err));
      }
    }
  );

  server.use(
    "/api/clients",
    mongooseCrudify({
      Model: Client,
      selectFields: "-__v", // Hide '__v' property
      endResponseInAction: false,

      beforeActions: [
        {
          middlewares: [requireAuth],
          except: ["list"],
        },
      ], // list (GET), create (POST), read (GET), update (PUT), delete (DELETE)
      afterActions: [{ middlewares: [helpers.formatResponse] }],
    })
  );
};
