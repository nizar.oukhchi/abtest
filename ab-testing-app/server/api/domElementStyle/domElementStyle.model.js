const mongoose = require("mongoose");

const domElementStyleSchema = new mongoose.Schema(
  {
    uid: {
      type: String,
      index: true,
      unique: true,
    },
    selector: String,
    uniqueSelector: String,
    style: {},
    content: String,
    src: String,
    nodeType: String,
    thompson: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ThompsonSampling",
    },
    isMultiple: { type: Boolean, default: false },
    useForConversion: { type: Boolean, default: false },
  },
  { toObject: { virtuals: true }, toJSON: { virtuals: true } }
);

domElementStyleSchema.methods.toJSON = function () {
  let obj = this.toObject();
  delete obj._id;
  delete obj.__v;
  return obj;
};

const DomElementStyle = mongoose.model(
  "DomElementStyle",
  domElementStyleSchema
);

module.exports = DomElementStyle;
