const boom = require("boom");

const DomElementStyle = require("./domElementStyle.model");
const requireSignIn = require("../../auth/requireSignIn");

module.exports = function (server) {
  server.post("/api/dom-element-styles/getAll", async (req, res, next) => {
    const { selector, abtest } = req.body;
    try {
      const elements = await DomElementStyle.find({
        selector,
        thompson: abtest,
      });

      if (elements.length > 0) return res.status(200).json(elements);
      else return res.status(200).json([]);
    } catch (err) {
      console.log(err);
    }
  });

  server.post("/api/dom-element-styles/:uid", async (req, res, next) => {
    const { uid } = req.params;
    const { style, content, isMultiple } = req.body;

    try {
      const element = await DomElementStyle.findOne({ uid });

      if (element) {
        element.style = style;
        element.content = content;
        element.isMultiple = isMultiple;
        await element.save();
        return res.status(200).json(element);
      } else {
        const {
          selector,
          abtest,
          style,
          content,
          src,
          nodeType,
          isMultiple,
        } = req.body;
        const createdElement = await DomElementStyle.create({
          uid,
          selector,
          thompson: abtest,
          content,
          src,
          style,
          nodeType,
          isMultiple,
        });
        return res.status(200).json(createdElement);
      }
    } catch (err) {
      console.log(err);
    }
  });

  server.put("/api/dom-element-styles", async (req, res, next) => {
    const {
      selector,
      newSelector,
      abtest,
      isMultiple,
      useForConversion,
    } = req.body;
    
    let domElements = [];
    try {
      if (useForConversion != null) {
        await DomElementStyle.updateMany(
          { selector, thompson: abtest },
          { $set: { useForConversion } }
        );
        domElements = await DomElementStyle.find({
          selector,
          thompson: abtest,
        });
      } else {
        await DomElementStyle.updateMany(
          { selector, thompson: abtest },
          { $set: { selector: newSelector, isMultiple, useForConversion } }
        );
        domElements = await DomElementStyle.find({
          selector: newSelector,
          thompson: abtest,
        });
      }
      return res.status(200).json(domElements);
    } catch (err) {
      console.log(err);
    }
  });

  server.delete("/api/dom-element-styles/:uid", async (req, res, next) => {
    const { uid } = req.params;

    try {
      await DomElementStyle.deleteOne({ uid });
      return res.status(200).json({ uid });
    } catch (err) {
      console.log(err);
    }
  });

  server.delete("/api/dom-element-styles", async (req, res, next) => {
    const { thompson } = req.query;
    try {
      if (!thompson || thompson === "null" || thompson === "undefined")
        return res.status(404).json("test not found");
      else {
        await DomElementStyle.deleteMany({ thompson });
        return res.status(200).json("ok");
      }
    } catch (err) {
      console.log(err);
    }
  });
};
