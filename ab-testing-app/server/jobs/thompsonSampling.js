const ThompsonSampling = require("../api/thompsonSampling/thompsonSampling.model");
const DomElementStyle = require("../api/domElementStyle/domElementStyle.model");

const filterBySelector = (array) => {
  let result = [];
  let alreadyTreated = [];

  for (let element of array) {
    if (alreadyTreated.indexOf(element.selector) === -1) {
      const chunk = array.filter((e) => e.selector === element.selector);
      result.push(chunk);
      alreadyTreated.push(element.selector);
    }
  }

  return result;
};

const cartesian = (inputArray) => {
  var r = [],
    arg = inputArray,
    max = arg.length - 1;
  function helper(arr, i) {
    for (var j = 0, l = arg[i].length; j < l; j++) {
      var a = arr.slice(0); // clone arr
      a.push(arg[i][j]);
      if (i == max) r.push(a);
      else helper(a, i + 1);
    }
  }
  helper([], 0);
  return r;
};

module.exports = (agenda) => {
  agenda.define("generate thompsonSampling variants", async (job) => {
    const { thompsonSamplingId, clientId } = job.attrs.data;

    const room = `${clientId}_notifications`;

    try {
      let thompsonSampling = await ThompsonSampling.findById(
        thompsonSamplingId
      );

      const domElements = await DomElementStyle.find({
        thompson: thompsonSamplingId,
      });

      const domElementsChunck = filterBySelector(domElements);
      const variants = cartesian(domElementsChunck).map((item, index) => {
        return {
          name: `variant #${index + 1}`,
          domElements: item,
        };
      });

      thompsonSampling.variants = variants;
      thompsonSampling.processing = false;
      thompsonSampling.variantsGenerated = true;
      thompsonSampling = await thompsonSampling.save();

      const notification = {
        type: "ab_test_generate_variants_job_done",
        payload: thompsonSampling,
      };

      agenda.io.emit(room, notification);

      return;
    } catch (err) {
      console.log(err);
    }
  });
};
