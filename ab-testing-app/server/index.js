require("dotenv").config();

const path = require("path");
const http = require("http");

const glob = require("glob");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const compression = require("compression");
const pino = require("express-pino-logger")();

const app = express();
const server = http.Server(app);
const port = process.env.PORT || 3001;

app.use(bodyParser.json());
app.use(compression());
app.use(pino);

app.use(cors());
app.options("*", cors());

require("./config/mongoose");

const agenda = require("./config/agenda");

require("./socket")(server, app, agenda);

const rootPath = path.normalize(`${__dirname}/..`);

glob.sync(`${rootPath}/server/api/**/index.js`).forEach(controllerPath =>
    require(controllerPath)(app)
);

require("./auth")(app);

app.use(express.static(path.resolve(__dirname, "..", "build")));

app.get("/*", function(req, res) {
    res.sendFile(path.join(__dirname, "..", "build", "index.html"));
});

app.use((err, req, res, next) => {
    console.log(err);
    if (err.isServer) {
        // req.log.error(err.output.payload);
    }
    return res.status(err.output.statusCode).json(err.output.payload);
});

server.listen(port, () => {
    console.log(`Server running on http://localhost:${port}/`);
});

module.exports = app;
