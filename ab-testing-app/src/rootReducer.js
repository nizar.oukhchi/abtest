import { combineReducers } from "redux";

import { SlideReducer } from "./redux/Slide";
import { AuthReducer } from "./redux/Auth";
import { AbtestsReducer } from "./redux/Abtests";
import { ClientsReducer } from "./redux/Clients";
import LoadingReducer from "./redux/Loading";

export default () =>
    combineReducers({
        slide: SlideReducer,
        auth: AuthReducer,
        abtests: AbtestsReducer,
        clients: ClientsReducer,
        loading: LoadingReducer
    });
