import { TOGGLE_SLIDE } from './types';

const initialState = {
  isOpen: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_SLIDE:
      return { isOpen: !state.isOpen };
    default:
      return state;
  }
};
