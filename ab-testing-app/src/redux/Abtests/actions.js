import {
  create,
  fetchAll,
  fetchOne,
  update,
  remove,
  generateVariants,
  updateVariantName
} from "api/abtest";

import {
  AB_TEST_CREATE_REQUEST,
  AB_TEST_CREATE_SUCCESS,
  AB_TEST_FETCH_ALL_REQUEST,
  AB_TEST_FETCH_ALL_SUCCESS,
  AB_TEST_FETCH_ONE_REQUEST,
  AB_TEST_FETCH_ONE_SUCCESS,
  AB_TEST_UPDATE_REQUEST,
  AB_TEST_UPDATE_SUCCESS,
  AB_TEST_REMOVE_REQUEST,
  AB_TEST_REMOVE_SUCCESS,
  AB_TEST_CLEAR_CURRENT,
  AB_TEST_GENERATE_VARIANTS_REQUEST,
  AB_TEST_GENERATE_VARIANTS_SUCCESS,
  AB_TEST_UPDATE_VARIANT_REQUEST,
  AB_TEST_UPDATE_VARIANT_SUCCESS,
} from "./types";

import { deauthenticate } from "redux/Auth/actions";

const createAction = (data) => (dispatch) => {
  dispatch({ type: AB_TEST_CREATE_REQUEST });
  create(data)
    .then((response) => {
      dispatch({
        type: AB_TEST_CREATE_SUCCESS,
        payload: response.data,
      });
    })
    .catch((err) => {
      if (err.status === 401) {
        deauthenticate()(dispatch);
      }
      console.log(err);
    });
};

const fetchAllAction = () => (dispatch) => {
  dispatch({ type: AB_TEST_FETCH_ALL_REQUEST });
  return fetchAll()
    .then((response) => {
      dispatch({
        type: AB_TEST_FETCH_ALL_SUCCESS,
        payload: response.data,
      });
      return Promise.resolve(response.data);
    })
    .catch((err) => {
      if (err.response.status === 401) {
        deauthenticate()(dispatch);
      }
      console.log(err);
      //throw new Error(err);
    });
};

const fetchOneAction = (id) => (dispatch) => {
  dispatch({ type: AB_TEST_FETCH_ONE_REQUEST });
  return fetchOne(id)
    .then((response) => {
      dispatch({
        type: AB_TEST_FETCH_ONE_SUCCESS,
        payload: response.data,
      });
      return Promise.resolve(response.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

const updateAction = (item) => (dispatch) => {
  dispatch({ type: AB_TEST_UPDATE_REQUEST });
  return update(item)
    .then((response) => {
      dispatch({
        type: AB_TEST_UPDATE_SUCCESS,
        payload: response.data,
      });
      return Promise.resolve(response.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

const removeAction = (id) => (dispatch) => {
  dispatch({ type: AB_TEST_REMOVE_REQUEST });
  return remove(id)
    .then((response) => {
      dispatch({
        type: AB_TEST_REMOVE_SUCCESS,
        payload: response.data,
      });
      return Promise.resolve(response.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

const clearCurrentAction = () => (dispatch) => {
  dispatch({
    type: AB_TEST_CLEAR_CURRENT,
  });
};

const generateVariantsAction = (id) => (dispatch) => {
  dispatch({ type: AB_TEST_GENERATE_VARIANTS_REQUEST });
  return generateVariants(id)
    .then((response) => {
      dispatch({
        type: AB_TEST_GENERATE_VARIANTS_SUCCESS,
        payload: response.data,
      });
      return Promise.resolve(response.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

const updateVariantNameAction = ({ id, name }) => (dispatch) => {
  dispatch({ type: AB_TEST_UPDATE_VARIANT_REQUEST });
  return updateVariantName({id, name})
    .then((response) => {
      dispatch({
        type: AB_TEST_UPDATE_VARIANT_SUCCESS,
        payload: response.data,
      });
      return Promise.resolve(response.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

export {
  createAction,
  fetchAllAction,
  fetchOneAction,
  updateAction,
  removeAction,
  clearCurrentAction,
  generateVariantsAction,
  updateVariantNameAction
};
