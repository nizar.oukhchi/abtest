import {
  AB_TEST_CREATE_SUCCESS,
  AB_TEST_FETCH_ALL_SUCCESS,
  AB_TEST_FETCH_ONE_SUCCESS,
  AB_TEST_UPDATE_SUCCESS,
  AB_TEST_REMOVE_SUCCESS,
  AB_TEST_CLEAR_CURRENT,
  AB_TEST_GENERATE_VARIANTS_SUCCESS,
  AB_TEST_GENERATE_VARIANTS_JOB_DONE,
  AB_TEST_UPDATE_VARIANT_SUCCESS,
} from "./types";

const initialState = {
  current: {},
  list: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AB_TEST_FETCH_ALL_SUCCESS:
      return { ...state, list: action.payload };
    case AB_TEST_CREATE_SUCCESS:
    case AB_TEST_FETCH_ONE_SUCCESS:
    case AB_TEST_UPDATE_SUCCESS:
    case AB_TEST_GENERATE_VARIANTS_SUCCESS:
    case AB_TEST_GENERATE_VARIANTS_JOB_DONE:
    case AB_TEST_UPDATE_VARIANT_SUCCESS:
      return { ...state, current: action.payload };
    case AB_TEST_REMOVE_SUCCESS:
    case AB_TEST_CLEAR_CURRENT:
      return { ...state, current: {} };
    default:
      return state;
  }
};
