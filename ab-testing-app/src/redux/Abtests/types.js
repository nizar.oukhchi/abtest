export const AB_TEST_CREATE_REQUEST = "ab_test_create_request";
export const AB_TEST_CREATE_SUCCESS = "ab_test_create_success";
export const AB_TEST_CREATE_FAILURE = "ab_test_create_failure";
export const AB_TEST_FETCH_ALL_REQUEST = "ab_test_fetch_all_request";
export const AB_TEST_FETCH_ALL_SUCCESS = "ab_test_fetch_all_success";
export const AB_TEST_FETCH_ALL_FAILURE = "ab_test_fetch_all_failure";
export const AB_TEST_FETCH_ONE_REQUEST = "ab_test_fetch_one_request";
export const AB_TEST_FETCH_ONE_SUCCESS = "ab_test_fetch_one_success";
export const AB_TEST_FETCH_ONE_FAILURE = "ab_test_fetch_one_failure";
export const AB_TEST_UPDATE_REQUEST = "ab_test_update_request";
export const AB_TEST_UPDATE_SUCCESS = "ab_test_update_success";
export const AB_TEST_UPDATE_FAILURE = "ab_test_update_failure";
export const AB_TEST_REMOVE_REQUEST = "ab_test_remove_request";
export const AB_TEST_REMOVE_SUCCESS = "ab_test_remove_success";
export const AB_TEST_CLEAR_CURRENT = "ab_test_clear_current";
export const AB_TEST_GENERATE_VARIANTS_REQUEST =
  "ab_test_generate_variants_request";
export const AB_TEST_GENERATE_VARIANTS_SUCCESS =
  "ab_test_generate_variants_success";
export const AB_TEST_GENERATE_VARIANTS_FAILURE =
  "ab_test_generate_variants_failure";
export const AB_TEST_GENERATE_VARIANTS_JOB_DONE =
  "ab_test_generate_variants_job_done";
export const AB_TEST_UPDATE_VARIANT_REQUEST = "ab_test_update_variant_request";
export const AB_TEST_UPDATE_VARIANT_SUCCESS = "ab_test_update_variant_success";
