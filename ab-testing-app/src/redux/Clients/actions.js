import axios from 'axios';

import { CLIENT_FETCH_ALL_REQUEST, CLIENT_FETCH_ALL_SUCCESS } from './types';

const fetchAll = ({ abtest }) => dispatch => {
  dispatch({ type: CLIENT_FETCH_ALL_REQUEST });
  return axios
    .get(`/api/clients?abtest=${abtest}`)
    .then(response => {
      dispatch({
        type: CLIENT_FETCH_ALL_SUCCESS,
        payload: response.data
      });
      return Promise.resolve(response.data);
    })
    .catch(err => {
      throw new Error(err);
    });
};

export { fetchAll };
