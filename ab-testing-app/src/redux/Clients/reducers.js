import { CLIENT_FETCH_ALL_REQUEST, CLIENT_FETCH_ALL_SUCCESS } from './types';

const initialState = {
  list: [],
  isLoading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CLIENT_FETCH_ALL_REQUEST:
      return { ...state, isLoading: true };
    case CLIENT_FETCH_ALL_SUCCESS:
      return { ...state, list: action.payload, isLoading: false };
    default:
      return state;
  }
};
