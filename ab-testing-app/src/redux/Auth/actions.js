import { authenticate, getCurrentUser } from 'api/auth';

import { TOGGLE_SLIDE } from '../Slide/types';
import { setAuthToken, removeAuthToken } from 'utils/localStorage';

import {
    AUTHENTICATE_REQUEST,
    AUTHENTICATE_FAILURE,
    AUTHENTICATE_SUCCESS,
    DEAUTHENTICATE,
    GET_USER_REQUEST,
    GET_USER_REQUEST_SUCCESS,
    GET_USER_REQUEST_ERROR
} from './types';

// gets token from the api and stores it in the redux store and in cookie
const authenticateAction = (
    { email, password, name, googleId, accountName },
    type
) => dispatch => {
    dispatch({ type: AUTHENTICATE_REQUEST });
    authenticate({ email, password, name, googleId, accountName }, type)
        .then(response => {
            setAuthToken(response.data.token);
            dispatch({ type: AUTHENTICATE_SUCCESS, payload: response.data });
        })
        .catch(err => {
            dispatch({
                type: AUTHENTICATE_FAILURE,
                payload: 'Authentification error'
            });
        });
};

// gets the token from the cookie and saves it in the store
const reauthenticate = token => dispatch => {
    dispatch({ type: AUTHENTICATE_SUCCESS, payload: token });
};

// removing the token
const deauthenticate = () => dispatch => {
    removeAuthToken('token');
    dispatch({ type: DEAUTHENTICATE });
    dispatch({ type: TOGGLE_SLIDE });
};

const getCurrentUserAction = () => dispatch => {
    dispatch({ type: GET_USER_REQUEST });
    getCurrentUser()
        .then(response => {
            dispatch({
                type: GET_USER_REQUEST_SUCCESS,
                payload: response.data.user
            });
        })
        .catch(err => {
            dispatch({
                type: GET_USER_REQUEST_ERROR,
                payload: 'getCurrentUser api error'
            });
        });
};

export {
    authenticateAction,
    reauthenticate,
    deauthenticate,
    getCurrentUserAction
};
