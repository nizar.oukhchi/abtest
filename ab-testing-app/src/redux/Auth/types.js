export const AUTHENTICATE_REQUEST = 'authenticate_request';
export const AUTHENTICATE_FAILURE = 'authenticate_failure';
export const AUTHENTICATE_SUCCESS = 'authenticate_success';
export const DEAUTHENTICATE = 'deauthenticate';
export const GET_USER_REQUEST = 'get_user_request';
export const GET_USER_REQUEST_SUCCESS = 'get_user_success';
export const GET_USER_REQUEST_ERROR = 'get_user_failure';
