import {
    AUTHENTICATE_REQUEST,
    AUTHENTICATE_FAILURE,
    AUTHENTICATE_SUCCESS,
    DEAUTHENTICATE,
    GET_USER_REQUEST,
    GET_USER_REQUEST_SUCCESS,
    GET_USER_REQUEST_ERROR
} from "./types";

const initialState = {
    token: localStorage.getItem("TOKEN") || "",
    user: {},
    error: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case AUTHENTICATE_REQUEST:
            return { ...state, error: null };
        case AUTHENTICATE_SUCCESS:
            return {
                ...state,
                token: action.payload.token,
                user: action.payload.user,
                error: null
            };
        case AUTHENTICATE_FAILURE:
            return { ...state, error: action.payload };
        case DEAUTHENTICATE:
            return { ...state, token: null };
        case GET_USER_REQUEST:
            return { ...state, error: null };
        case GET_USER_REQUEST_SUCCESS:
            return {
                ...state,
                user: action.payload,
                error: null
            };
        case GET_USER_REQUEST_ERROR:
            return { ...state, error: action.payload };
        default:
            return state;
    }
};
