import axios from 'axios';
import { getAuthToken } from 'utils/localStorage';

const fetchClient = () => {
    const defaultOptions = {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    // Create instance
    let instance = axios.create(defaultOptions);

    // Set the AUTH token for any request
    instance.interceptors.request.use(config => {
        const token = getAuthToken('token');
        config.headers.Authorization = token ? `Bearer ${token}` : '';
        return config;
    });

    /*     instance.interceptors.response.use(
        response => {
            return response;
        },
        error => {
            if (error.response) {
                switch (error.response.status) {
                    case 401:
                        debugger;
                        //save current route
                        setRouteRedirect(window.location.pathname);

                        //construct the redirect url after authentication to save token
                        let redirectAfterAuth =
                            window.location.origin + '/authenticate';

                        //go to the auth service
                        window.location = REQUEST_AUTH_URL + redirectAfterAuth;
                        break;
                    default:
                        break;
                }
            }

            return Promise.reject(error);
        }
    ); */

    return instance;
};

export default fetchClient();
