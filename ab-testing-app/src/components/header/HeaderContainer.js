import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Header from './HeaderComponent';
import { clearCurrentAction } from 'redux/Abtests/actions';

const mapStateToProps = state => ({
    abTest: state.abtests.current,
    isLoading: state.abtests.isLoading
});

const mapDispatchToProps = dispatch => ({
    clearCurrentAbTest: bindActionCreators(clearCurrentAction, dispatch)
});

const HeaderContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);

export default HeaderContainer;
