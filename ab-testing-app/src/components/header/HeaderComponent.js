import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import { NavLink } from "react-router-dom";

const HeaderComponent = ({
  abTest,
  clearCurrentAbTest,
  toggleSlide,
  isOpen,
  location,
  history,
}) => {
  const { pathname } = location;

  const backButtonClickHandler = () => {
    clearCurrentAbTest();
    history.push("/");
  };

  return (
    <div className="flex flex-col">
      <div className="flex items-center bg-gradient-rb-blue-purple py-4 px-4 lg:px-8">
        <a className="text-2xl text-white cursor-pointer" onClick={toggleSlide}>
          {isOpen ? (
            <i className="typcn typcn-th-large-outline" />
          ) : (
            <i className="typcn typcn-th-large" />
          )}
        </a>

        <div className="ml-auto">
          <a className="text-2xl text-white mr-6">
            <i className="typcn typcn-bell" />
          </a>
          <a className="text-2xl text-white">
            <i className="typcn typcn-user-outline" />
          </a>
        </div>
      </div>
      {pathname.indexOf("abtest") !== -1 && (
        <div className="flex flex:col items-center border-b border-grey-light bg-white px-4 lg:px-8">
          {abTest && (
            <React.Fragment>
              <a
                className="text-grey-dark no-underline uppercase font-bold cursor-pointer"
                onClick={backButtonClickHandler}
              >
                <i className="typcn typcn-arrow-left-thick text-xl mr-1" />
                back to list
              </a>

              <ul className="flex flex-row list-reset ml-auto">
                <NavLink
                  className="header-link"
                  activeClassName="active"
                  to={`/abtest/${abTest.id}/details`}
                >
                  Details
                </NavLink>
                <NavLink
                  className="header-link"
                  activeClassName="active"
                  to={`/abtest/${abTest.id}/variants`}
                >
                  Variants
                </NavLink>
                <NavLink
                  className="header-link"
                  activeClassName="active"
                  to={`/abtest/${abTest.id}/conversion`}
                >
                  Conversion
                </NavLink>
                <NavLink
                  className={
                    "header-link" +
                    (abTest.status === "DRAFT" ? " disabled" : "")
                  }
                  activeClassName="active"
                  to={`/abtest/${abTest.id}/statistics`}
                >
                  Statistics
                </NavLink>
                <NavLink
                  className="header-link"
                  activeClassName="active"
                  to={`/abtest/${abTest.id}/settings`}
                >
                  Settings
                </NavLink>
              </ul>
            </React.Fragment>
          )}
        </div>
      )}

      {pathname.indexOf("profile") !== -1 && (
        <div className="flex flex:col items-center border-b border-grey-light bg-white px-4 lg:px-8">
          <ul className="flex flex-row list-reset">
            <NavLink
              className="header-link"
              activeClassName="active"
              to={`/profile/details`}
            >
              Details
            </NavLink>
            <NavLink
              className="header-link"
              activeClassName="active"
              to={`/profile/update-password`}
            >
              Update password
            </NavLink>
            <NavLink
              className="header-link"
              activeClassName="active"
              to={`/profile/team`}
            >
              Account management
            </NavLink>
          </ul>
        </div>
      )}
    </div>
  );
};

HeaderComponent.propTypes = {
  abTest: PropTypes.object,
  toggleSlide: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
};

HeaderComponent.defaultProps = {
  abTest: {},
};

export default withRouter(HeaderComponent);
