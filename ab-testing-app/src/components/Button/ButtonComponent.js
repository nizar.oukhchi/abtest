import React from "react";
import PropTypes from "prop-types";
import { SyncLoader } from "react-spinners";

const ButtonComponent = ({
  children,
  btnStyle,
  isLoading,
  className,
  disabled,
  ...props
}) => {
  const _className = `${btnStyle} ${className}`;

  const renderLoader = (childrenToRender, color) =>
    isLoading ? <SyncLoader size={8} color={color} /> : childrenToRender();

  return (
    <button {...props} className={_className} disabled={isLoading || disabled}>
      {renderLoader(
        () => children,
        btnStyle === "btn-outline" ? "white" : "#3e7bf7"
      )}
    </button>
  );
};

export default ButtonComponent;

ButtonComponent.propTypes = {
  children: PropTypes.string,
  btnStyle: PropTypes.string,
  isLoading: PropTypes.bool,
  className: PropTypes.string,
  disabled: PropTypes.bool,
};

ButtonComponent.defaultProps = {
  children: "",
  btnStyle: "btn",
  isLoading: false,
  className: "",
  disabled: false,
};
