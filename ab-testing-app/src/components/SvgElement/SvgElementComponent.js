import React from "react";
import SVG from "react-inlinesvg";

import EmptyIlustration from "../../assets/img/empty.svg";
import ElementsIlustration from "../../assets/img/elements.svg";
import FactoryIlustration from "../../assets/img/factory.svg";

const SvgElementComponent = ({ ilustrationName }) => {
    switch (ilustrationName) {
        case "empty":
            return <SVG src={EmptyIlustration} />;
        case "elements":
            return <SVG src={ElementsIlustration} />;
        case "factory":
            return <SVG src={FactoryIlustration} />;

        default:
            return <SVG src={EmptyIlustration} />;
    }
};

export default SvgElementComponent;
