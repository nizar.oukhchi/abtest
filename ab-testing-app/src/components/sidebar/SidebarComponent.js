import React from "react";

import Avatar from "./Avatar";
import Menu from "./Menu";
import AccountSelector from "./AccountSelector";

import "./sidebar.css";

const SidebarComponent = ({ logout, user }) => {
  return (
    <div className="absolute h-full w-4/5 lg:w-1/5 bg-gradient-rb-blue-purple px-4">
      <div className="flex items-center py-4 mb-2">
        <a className="text-xl text-white py-1">Logo</a>
      </div>
      <Avatar name={user.name || user.email} />
      <AccountSelector user={user} />
      <div className="text-white">
        Your api key is{" "}
        <span className="text-bold uppercase">{user.account?.clientKey}</span>
      </div>
      <Menu logout={logout} />
    </div>
  );
};

export default SidebarComponent;
