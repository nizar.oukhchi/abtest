import React from "react";
import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";

import { TOGGLE_SLIDE } from "redux/Slide/types";

const MenuComponent = ({ logout }) => {
  const dispatch = useDispatch();

  const closeSidebar = () => {
    dispatch({ type: TOGGLE_SLIDE });
  };
  return (
    <div className="flex flex-col mt-8">
      <span className="text-xs text-white font-bold uppercase mb-6">Menu</span>
      <ul className="flex flex-col list-reset pl-4">
        <li className="text-base mb-4 uppercase">
          <NavLink
            to="/"
            exact
            className="text-grey-light hover:text-white no-underline cursor-pointer"
            activeClassName="font-bold"
            onClick={closeSidebar}
          >
            <i className="typcn typcn-flash-outline mr-2" />
            Campaigns
          </NavLink>
        </li>
        <li className="text-base mb-4 uppercase">
          <NavLink
            to="/profile"
            exact
            className="text-grey-light hover:text-white no-underline cursor-pointer"
            activeClassName="font-bold"
            onClick={closeSidebar}
          >
            <i className="typcn typcn-user-outline mr-2" />
            Profile
          </NavLink>
        </li>
        <li className="text-base mb-4 uppercase">
          <a
            className="text-grey-light hover:text-white no-underline cursor-pointer"
            onClick={logout}
          >
            <i className="typcn typcn-power-outline mr-2" />
            Logout
          </a>
        </li>
      </ul>
    </div>
  );
};

export default MenuComponent;
