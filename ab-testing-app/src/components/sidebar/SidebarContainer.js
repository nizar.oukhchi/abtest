import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { deauthenticate } from '../../redux/Auth';

import SidebarComponent from './SidebarComponent';

const mapStateToProps = state => ({
    user: state.auth.user
});

const mapDispatchToProps = dispatch => ({
    logout: bindActionCreators(deauthenticate, dispatch)
});

const SidebarContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SidebarComponent);

export default SidebarContainer;
