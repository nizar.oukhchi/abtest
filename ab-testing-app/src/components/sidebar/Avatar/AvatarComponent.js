import React from 'react';
import Avatar from 'react-avatar';

const AvatarComponent = ({ name }) => (
  <div className="flex flex-col items-center p-4">
    <Avatar
      name={name}
      size="135"
      round={true}
      color="#fff"
      fgColor="#2991fc"
    />
    <div className="text-2xl text-white mt-6">{name}</div>
  </div>
);

export default AvatarComponent;
