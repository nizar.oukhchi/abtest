import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useToasts } from "react-toast-notifications";

import fetchClient from "../../../utils/fetchClient";
import { GET_USER_REQUEST_SUCCESS } from "../../../redux/Auth/types";
import { fetchAllAction } from "redux/Abtests/actions";
import Select from "../../Select";

const AccountSelector = ({ user }) => {
  const dispatch = useDispatch();
  const { addToast } = useToasts();
  const [currentAccount, setCurrentAccount] = useState();

  useEffect(() => {
    if (user.account) {
      setCurrentAccount(user.account._id);
    }
  }, [user]);

  const onAccountSelected = async (value) => {
    try {
      const response = await fetchClient.put(`/api/clients/set-account`, {
        account: value,
      });
      dispatch({
        type: GET_USER_REQUEST_SUCCESS,
        payload: response.data,
      });
      dispatch(fetchAllAction());
      addToast(
        <div className="flex items-center p-2">
          <p className="text-xl">Current account changed with success</p>
        </div>,
        {
          appearance: "success",
          autoDismiss: true,
        }
      );
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="mt-2 mb-6">
      <label className="inline-block text-white mb-2">Selected account</label>
      <Select
        value={currentAccount}
        onChange={(e) => {
          setCurrentAccount(e.target.value);
          onAccountSelected(e.target.value);
        }}
      >
        {user.accounts &&
          user.accounts.map((item) => (
            <option key={item?._id} value={item?._id}>
              {item?.name}
            </option>
          ))}
      </Select>
    </div>
  );
};

export default AccountSelector;
