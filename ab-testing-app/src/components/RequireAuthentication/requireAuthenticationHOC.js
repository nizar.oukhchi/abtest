import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';

const requireAuthentication = Component => {
    const AuthenticatedComponent = ({ isAuthenticated, ...props }) => {
        return isAuthenticated ? (
            <Component {...props} />
        ) : (
            <Redirect to="/login" />
        );
    };

    const mapStateToProps = state => ({
        isAuthenticated: !!state.auth.token
    });

    return connect(mapStateToProps)(AuthenticatedComponent);
};

export default requireAuthentication;
