import { connect } from 'react-redux';

import { TOGGLE_SLIDE } from '../../redux/Slide/types';

import MainLayoutComponent from './MainLayoutComponent';
import RequireAuthentication from '../RequireAuthentication';

const mapStateToProps = state => ({
    isOpen: state.slide.isOpen
});

const mapDispacthToProps = dispatch => ({
    toggleSlide: () => dispatch({ type: TOGGLE_SLIDE })
});

const MainLayoutContainer = connect(
    mapStateToProps,
    mapDispacthToProps
)(MainLayoutComponent);

export default RequireAuthentication(MainLayoutContainer);
