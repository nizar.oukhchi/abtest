import React from 'react';
import { Route } from 'react-router-dom';

import Sidebar from '../sidebar';
import Header from '../header';

const MainLayoutComponent = ({
    component: Component,
    toggleSlide,
    isOpen,
    ...rest
}) => (
    <Route
        {...rest}
        render={matchProps => (
            <React.Fragment>
                <Sidebar />
                <div className={'main-layout' + (isOpen ? ' slide' : '')}>
                    <Header toggleSlide={toggleSlide} isOpen={isOpen} />
                    <div className="flex flex-1 bg-grey-lightest h-full px-2 lg:px-10 py-4 lg:py-6">
                        <Component {...matchProps} />
                    </div>
                    <div
                        className={
                            'absolute pin-y pin-x bg-black opacity-50 cursor-pointer' +
                            (isOpen ? '' : ' hidden')
                        }
                        onClick={toggleSlide}
                    />
                </div>
            </React.Fragment>
        )}
    />
);

export default MainLayoutComponent;
