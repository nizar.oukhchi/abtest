import React from "react";
import PropTypes from "prop-types";
import { Route } from "react-router-dom";

const AuthLayoutComponent = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={matchProps => (
            <div className="flex h-full">
                <div className="hidden lg:flex flex-1 items-center justify-center bg-gradient-rb-blue-purple p-8">
                    <div className="text-center mb-6">
                        <h2 className="text-3xl text-white mb-4">
                            Testing solution
                        </h2>
                        <p className="text-xl text-white">
                            A proof of concept app, a solution that helps you do
                            your A/B testing the easiest way possible.
                        </p>
                    </div>
                </div>

                <div className="flex-1 p-4 lg:p-8">
                    <Component {...matchProps} />
                </div>
            </div>
        )}
    />
);

AuthLayoutComponent.propTypes = {
    component: PropTypes.object.isRequired
};

export default AuthLayoutComponent;
