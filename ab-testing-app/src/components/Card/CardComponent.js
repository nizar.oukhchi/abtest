import React from "react";
import PropTypes from "prop-types";

const CardComponent = ({ className, children, style }) => (
    <div className={`card ${className || ""}`} style={style}>
        {children}
    </div>
);

export default CardComponent;

CardComponent.propTypes = {
    children: PropTypes.any.isRequired,
    className: PropTypes.string
};

CardComponent.defaultProps = {
    className: ""
};
