import React from "react";
import PropTypes from "prop-types";

const Input = ({
  label,
  type,
  name,
  value,
  className,
  inputType,
  error,
  isInvalid,
  ...props
}) => {
  const _className = `input-container${className ? " " + className : ""}`;

  return (
    <div className={_className}>
      <label>{label}</label>
      {inputType === "textarea" ? (
        <textarea type={type} name={name} value={value} {...props} />
      ) : (
        <input type={type} name={name} value={value} {...props} />
      )}
      {isInvalid && <span className="text-red-darker">{error}</span>}
    </div>
  );
};

export default Input;

Input.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  inputType: PropTypes.string,
  value: PropTypes.string,
  className: PropTypes.string,
  error: PropTypes.string,
};

Input.defaultProps = {
  label: "",
  type: "btn",
  value: "",
  inputType: "input",
  className: "",
  error: ""
};
