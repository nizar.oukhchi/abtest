import React from "react";
import ReactDOM from "react-dom";
import { ModalContext } from "./ModalContext";

const Modal = () => {
  let { modalContent, handleModal, modal } = React.useContext(ModalContext);
  if (modal) {
    return ReactDOM.createPortal(
      <div
        className="modal fixed top-0 left-0 h-screen w-full flex items-center justify-center z-50"
        style={{
          background: "rgba(255,255,255,.2)",
          backdropFilter: "saturate(180%) blur(3px)",
        }}
      >
        <div className="bg-white relative p-5 bg-white border border-grey-lighter rounded overflow-hidden shadow flex flex-col items-start text-lg text-gray-800">
          <button
            className="absolute text-2xl top-0 right-0 -mt-12 font-bold self-end rounded-full bg-red-200 mb-3 bg-white text-red-700 w-8 h-8"
            onClick={() => handleModal()}
          >
            <i className="typcn typcn-times" />
          </button>
          <div>{modalContent}</div>
        </div>
      </div>,
      document.querySelector("#modal-root")
    );
  } else return null;
};

export default Modal;
