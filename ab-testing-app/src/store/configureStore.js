import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import rootReducer from '../rootReducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore() {
    const middlewares = [thunk];

    if (process.env.NODE_ENV !== 'production') {
        const logger = createLogger();
        middlewares.push(logger);
    }

    const store = createStore(
        rootReducer(),
        composeEnhancers(applyMiddleware(...middlewares))
    );

    store.subscribe(() => {
        const token = store.getState().auth.token;
        if (token) {
            localStorage.setItem('TOKEN', store.getState().auth.token);
        } else {
            localStorage.removeItem('TOKEN');
        }
    });

    return store;
}
