import React, { useEffect } from "react";
import { Provider, useSelector, useDispatch } from "react-redux";
import useSocket from "use-socket.io-client";
import { ToastProvider } from "react-toast-notifications";

import configureStore from "./store/configureStore";
import routes from "./routes";
import { getCurrentUserAction } from "./redux/Auth/actions";
import { getAuthToken } from "./utils/localStorage";
import { ModalProvider } from "./components/Modal";

const url =
  process.env.NODE_ENV === "development"
    ? "http://localhost:3001"
    : "https://vast-peak-74749.herokuapp.com";
const store = configureStore();

if (getAuthToken()) {
  store.dispatch(getCurrentUserAction());
}

const Socket = ({ children }) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.auth.user);

  const [socket] = useSocket(url, {
    autoConnect: true,
  });

  socket.connect();

  socket.on("connect", () => {
    console.log("Connected to AB testing platform");
  });

  socket.on("disconnect", () => {
    console.log("Disconnect from AB testing platform");
  });

  useEffect(() => {
    if (user._id) {
      const room = `${user._id}_notifications`;
      socket.emit("subscribe", room);

      socket.on(room, (action) => {
        dispatch(action);
      });
    }
  }, [user]);

  return <>{children}</>;
};

const App = () => {
  return (
    <Provider store={store}>
      <ModalProvider>
        <ToastProvider
          autoDismiss
          autoDismissTimeout={6000}
          placement="top-right"
        >
          <Socket>{routes}</Socket>
        </ToastProvider>
      </ModalProvider>
    </Provider>
  );
};

export default App;
