import React from "react";
import Moment from "react-moment";
import { Link } from "react-router-dom";

import Card from "components/Card";

const AbTestCardComponent = ({
  id,
  name,
  status,
  url,
  updatedAt,
  description,
  creator,
}) => {
  const abTestDetailsUrl = `/abtest/${id}/details`;
  const abTestVariantsUrl = `/abtest/${id}/variants`;
  const abTestStatisticsUrl = `/abtest/${id}/statistics`;
  const abTestSettingsUrl = `/abtest/${id}/settings`;

  return (
    <Card style={{ height: 310 }} className="flex flex-col">
      <div className="card-content">
        <Link className="card-title truncate" to={abTestDetailsUrl}>
          {name}
        </Link>
        <a className="card-subtitle mt-3">
          <i className="text-base typcn typcn-key-outline mr-1" />
          {id}
        </a>
        <a className="card-subtitle mt-1">
          <i className="text-base typcn typcn-export-outline mr-1" />
          {url}
        </a>
        <a className="card-subtitle mt-1">
          <i className="text-base typcn typcn-calendar-outline mr-1" />
          updated
          <span className="ml-1">
            <Moment format="MMMM Do YYYY">{updatedAt}</Moment>
          </span>
        </a>
        <a className="card-subtitle mt-1">
          <i className="text-base typcn typcn-group-outline mr-1" />
          <span className="ml-1">{creator?.name}</span>
        </a>
        {description && (
          <p className="text-grey-darker text-base mt-4 text-justify">
            {description}
          </p>
        )}
      </div>
      <div className="card-footer mt-auto">
        <span className={status === "LIVE" ? "flag-green" : "flag-orange"}>
          {status}
        </span>
        <div className="ml-auto">
          <Link
            to={abTestDetailsUrl}
            className="text-xl text-grey-darker  cursor-pointer no-underline mr-3"
          >
            <i className="typcn typcn-edit" />
          </Link>
          <Link
            to={abTestVariantsUrl}
            className="text-xl text-grey-darker  cursor-pointer no-underline mr-3"
          >
            <i className="typcn typcn-flow-merge" />
          </Link>
          <Link
            to={abTestStatisticsUrl}
            className="text-xl text-grey-darker  cursor-pointer no-underline mr-3"
          >
            <i className="typcn typcn-chart-bar-outline" />
          </Link>
          <Link
            to={abTestSettingsUrl}
            className="text-xl text-grey-darker  cursor-pointer no-underline mr-3"
          >
            <i className="typcn typcn-cog-outline" />
          </Link>
        </div>
      </div>
    </Card>
  );
};

export default AbTestCardComponent;
