import React, { useEffect } from "react";
import { Redirect } from "react-router";

import AbTestCard from "./abTestCard";
import AddTestCard from "./addTestCard";

const IndexComponent = ({
    isLoading,
    createAbTest,
    abTestList,
    currentAbTest,
    fetchAllAbTests
}) => {
    useEffect(() => {
        fetchAllAbTests();
    }, []);

    return !currentAbTest.id ? (
        <React.Fragment>
            <div className="flex flex-1 flex-wrap">
                {abTestList.map(item => (
                    <div key={item.id} className="lg:w-1/4 px-2">
                        <AbTestCard {...item} />
                    </div>
                ))}
                <div className="lg:w-1/4 px-2">
                    <AddTestCard
                        createAbTest={createAbTest}
                        isLoading={isLoading}
                    />
                </div>
            </div>
        </React.Fragment>
    ) : (
        <Redirect to={`/abtest/${currentAbTest.id}/details`} />
    );
};

export default IndexComponent;
