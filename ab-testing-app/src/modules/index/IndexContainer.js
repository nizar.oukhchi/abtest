import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { createAction, fetchAllAction } from "../../redux/Abtests";
import { createLoadingSelector } from "../../redux/Loading";

import IndexComponent from "./IndexComponent";

const loadingSelector = createLoadingSelector(["ab_test_create"]);

const mapStateToProps = state => ({
    isLoading: loadingSelector(state),
    abTestList: state.abtests.list,
    currentAbTest: state.abtests.current
});

const mapDispatchToProps = dispatch => ({
    createAbTest: bindActionCreators(createAction, dispatch),
    fetchAllAbTests: bindActionCreators(fetchAllAction, dispatch)
});

const IndexContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(IndexComponent);

export default IndexContainer;
