import React, { useState, useEffect } from "react";
import { SyncLoader } from "react-spinners";

import Card from "components/Card";
import Input from "components/Input";
import Button from "components/Button";

const urlRegex = /(https?:\/\/(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9])(:?\d*)\/?([a-z_\/0-9\-#.]*)\??([a-z_\/0-9\-#=&]*)/;

const AddTestCardComponent = ({ createAbTest, isLoading }) => {
    const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
    const [url, setUrl] = useState("");
    const [urlError, setUrlError] = useState("");

    useEffect(() => {
        if (url) {
            if (!urlRegex.test(url)) {
                setUrlError("url should be valid");
            } else {
                setUrlError("");
            }
        }
    }, [url]);

    const openCreateModal = () => {
        setIsCreateModalOpen(true);
    };

    const closeCreateModal = () => {
        setIsCreateModalOpen(false);
    };

    const createAbtestHandler = () => {
        if (url !== "") {
            createAbTest({ url });
            closeCreateModal();
        }
    };

    return (
        <Card style={{ height: 310 }}>
            <div className="card-content relative">
                <div className="p-5">
                    <button
                        type="button"
                        onClick={openCreateModal}
                        disabled={isLoading}
                        className="flex items-center mx-auto text-blue cursor-pointer no-underline w-24 h-24 bg-blue-lightest rounded-full appearance-none focus:outline-none"
                    >
                        {isLoading ? (
                            <div className="mx-auto">
                                <SyncLoader size={8} color={"#2991fc"} />
                            </div>
                        ) : (
                            <i
                                className="relative text-5xl typcn typcn-plus mx-auto"
                                style={{ bottom: "2px", right: "1px" }}
                            />
                        )}
                    </button>
                    <div className="mt-8">
                        <h3 className="text-base text-center">Add new test</h3>
                        <p className="text-sm text-grey leading-normal text-center mt-3">
                            Create new test in your draft section where you can
                            add variants and publish
                        </p>
                    </div>
                </div>
                {isCreateModalOpen && (
                    <div className="absolute inset-0 bg-white">
                        <div>
                            <p className="text-sm text-grey leading-normal text-center mt-3 px-4">
                                Please insert a valid url of the web site you
                                want to test, with http or https in it
                            </p>
                            <div className="p-5">
                                <Input
                                    label="Website URL"
                                    value={url}
                                    onChange={e => {
                                        setUrl(e.target.value);
                                    }}
                                ></Input>
                                {urlError && (
                                    <p className="text-sm text-red-darker">
                                        {urlError}
                                    </p>
                                )}
                                <div className="flex flex-col mt-4">
                                    <Button
                                        className="block"
                                        onClick={createAbtestHandler}
                                        disabled={!url || urlError}
                                    >
                                        Create
                                    </Button>
                                    <Button
                                        btnStyle="btn-outline mt-2"
                                        onClick={() => {
                                            setUrl("");
                                            setIsCreateModalOpen(false);
                                        }}
                                    >
                                        Cancel
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </Card>
    );
};

export default AddTestCardComponent;
