import React from "react";
import { useSelector, useDispatch } from "react-redux";

import Input from "../../../components/Input";
import Button from "../../../components/Button";

const ProfileDetails = () => {
  const user = useSelector((state) => state.auth.user);
  return (
    <div className="flex flex-col w-full ">
      <h2 className="text-2xl text-grey-dark items-center pb-4 border-b border-grey-light mb-10 mt-6">
        Profile details
      </h2>
      <form
        className="flex flex-1 flex-col lg:flex-col"
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <Input
          label="Email"
          className="mt-2"
          type="text"
          name="email"
          value={user.email}
          readOnly
        />
        <Input
          label="Display name"
          className="mt-4"
          type="text"
          name="name"
          value={user.name}
        />
      </form>
    </div>
  );
};

export default ProfileDetails;
