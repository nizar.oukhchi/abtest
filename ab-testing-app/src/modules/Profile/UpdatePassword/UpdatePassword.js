import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from "react-toast-notifications";

import fetchClient from "../../../utils/fetchClient";
import { GET_USER_REQUEST_SUCCESS } from "../../../redux/Auth/types";

import Input from "../../../components/Input";
import Button from "../../../components/Button";

const UpdatePassword = () => {
  const dispatch = useDispatch();
  const { addToast } = useToasts();
  const user = useSelector((state) => state.auth.user);
  const formik = useFormik({
    initialValues: {
      oldPassword: "",
      newPassword: "",
      confirmPassword: "",
    },
    validationSchema: Yup.object({
      oldPassword: Yup.lazy(() => {
        if (user.isPasswordDefined)
          return Yup.string().required("The old password is required");
        return Yup.mixed().notRequired();
      }),
      newPassword: Yup.string().required("The new password is required"),
      confirmPassword: Yup.string()
        .oneOf([Yup.ref("newPassword")], "Passwords must match")
        .required("Confirm password is required"),
    }),
    onSubmit: async (values) => {
      try {
        const response = await fetchClient.post(
          `/api/clients/${user.id}/update-password`,
          values
        );
        dispatch({
          type: GET_USER_REQUEST_SUCCESS,
          payload: response.data,
        });
        addToast(
          <div className="flex items-center p-2">
            <p className="text-xl">Password updated with success</p>
          </div>,
          {
            appearance: "success",
            autoDismiss: true,
          }
        );
      } catch (err) {
        addToast(
          <div className="flex items-center p-2">
            <p className="text-xl">Old password doesn't match</p>
          </div>,
          {
            appearance: "error",
            autoDismiss: true,
          }
        );
      } finally {
        formik.setSubmitting(false);
        formik.resetForm();
      }
    },
  });

  return (
    <div className="flex flex-col w-full ">
      <h2 className="text-2xl text-grey-dark items-center pb-4 border-b border-grey-light mb-10 mt-6">
        Update password
      </h2>
      <form
        className="flex flex-1 flex-col lg:flex-col"
        onSubmit={formik.handleSubmit}
      >
        {user.isPasswordDefined && (
          <Input
            label="Old password"
            className="mt-4"
            type="password"
            name="oldPassword"
            isInvalid={formik.errors.oldPassword}
            error={formik.errors.oldPassword}
            onChange={formik.handleChange}
            value={formik.values.oldPassword}
          />
        )}

        <Input
          label="New password"
          className="mt-4"
          type="password"
          name="newPassword"
          isInvalid={formik.errors.newPassword}
          error={formik.errors.newPassword}
          onChange={formik.handleChange}
          value={formik.values.newPassword}
        />
        <Input
          label="Confirm new password"
          className="mt-4"
          type="password"
          name="confirmPassword"
          isInvalid={formik.errors.confirmPassword}
          error={formik.errors.confirmPassword}
          onChange={formik.handleChange}
          value={formik.values.confirmPassword}
        />
        <div className="flex mt-8">
          <Button
            type="submit"
            className="ml-auto"
            btnStyle="btn-outline"
            isLoading={formik.isSubmitting}
          >
            Update password
          </Button>
        </div>
      </form>
    </div>
  );
};

export default UpdatePassword;
