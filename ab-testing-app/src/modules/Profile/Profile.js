import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import ProfileDetails from "./ProfileDetails";
import UpdatePassword from "./UpdatePassword";
import Team from "./Team";

const Profile = () => {
  return (
    <Switch>
      <Route exact path="/profile/details" component={ProfileDetails} />
      <Route exact path="/profile/update-password" component={UpdatePassword} />
      <Route exact path="/profile/team" component={Team} />
      <Redirect from="/profile" to="/profile/details" />
    </Switch>
  );
};

export default Profile;
