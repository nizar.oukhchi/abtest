import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { useToasts } from "react-toast-notifications";

import fetchClient from "utils/fetchClient";
import { GET_USER_REQUEST_SUCCESS } from "redux/Auth/types";

import Input from "components/Input";
import Button from "components/Button";

const AccountForm = ({ account }) => {
  const { addToast } = useToasts();
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      name: account?.name,
    },
    validationSchema: Yup.object({
      name: Yup.string().required("The name is required"),
    }),
    onSubmit: async (values) => {
      try {
        const response = await fetchClient.put(
          `/api/accounts/${account?._id}`,
          values
        );
        dispatch({
          type: GET_USER_REQUEST_SUCCESS,
          payload: response.data,
        });
        addToast(
          <div className="flex items-center p-2">
            <p className="text-xl">Account updated with success</p>
          </div>,
          {
            appearance: "success",
            autoDismiss: true,
          }
        );
      } catch (err) {
        console.log(err);
      } finally {
        formik.setSubmitting(false);
        formik.resetForm();
      }
    },
    enableReinitialize: true,
  });

  return (
    <form
      className="flex flex-1 flex-col lg:flex-col"
      onSubmit={formik.handleSubmit}
    >
      <Input
        label="Account name"
        className="mt-4"
        type="name"
        name="name"
        value={account?.clientKey}
        readOnly={true}
      />
      <Input
        label="Account name"
        className="mt-4"
        type="name"
        name="name"
        isInvalid={formik.errors.name}
        error={formik.errors.name}
        onChange={formik.handleChange}
        value={formik.values.name}
      />
      <div className="flex mt-4">
        <Button
          type="submit"
          className="ml-auto"
          btnStyle="btn-outline"
          isLoading={formik.isSubmitting}
        >
          Update account details
        </Button>
      </div>
    </form>
  );
};

export default AccountForm;
