import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useToasts } from "react-toast-notifications";

import fetchClient from "utils/fetchClient";

import Input from "components/Input";
import Button from "components/Button";

const InviteForm = () => {
  const { addToast } = useToasts();

  const formik = useFormik({
    initialValues: {
      email: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email("This field must be a valid email")
        .required("The email of the user you want to invite is required"),
    }),
    onSubmit: async (values) => {
      try {
        await fetchClient.post(`/api/clients/invite`, values);
        addToast(
          <div className="flex items-center p-2">
            <p className="text-xl">Invitation sent with success</p>
          </div>,
          {
            appearance: "success",
            autoDismiss: true,
          }
        );
      } catch (err) {
        console.log(err);
      } finally {
        formik.setSubmitting(false);
        formik.resetForm();
      }
    },
  });

  return (
    <form
      className="flex flex-1 flex-col lg:flex-col"
      onSubmit={formik.handleSubmit}
    >
      <Input
        label="Email of the user you want to invite"
        className="mt-4"
        type="email"
        name="email"
        isInvalid={formik.errors.email}
        error={formik.errors.email}
        onChange={formik.handleChange}
        value={formik.values.email}
      />
      <div className="flex mt-4">
        <Button
          type="submit"
          className="ml-auto"
          btnStyle="btn-outline"
          isLoading={formik.isSubmitting}
        >
          Invite user to your account
        </Button>
      </div>
    </form>
  );
};

export default InviteForm;
