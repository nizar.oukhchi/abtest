import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from "react-toast-notifications";

import { GET_USER_REQUEST_SUCCESS } from "redux/Auth/types";

import fetchClient from "utils/fetchClient";

import Button from "components/Button";

import AccountForm from "./AccountForm";
import InviteForm from "./InviteForm";

const Team = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.auth.user);
  const { addToast } = useToasts();
  const [isLoading, setIsLoading] = useState(false);

  const acceptInvite = async () => {
    try {
      setIsLoading(true);
      const response = await fetchClient.get(`/api/clients/accept-invite`);
      dispatch({
        type: GET_USER_REQUEST_SUCCESS,
        payload: response.data,
      });
      addToast(
        <div className="flex items-center p-2">
          <p className="text-xl">Invitation accepted with success</p>
        </div>,
        {
          appearance: "success",
          autoDismiss: true,
        }
      );
    } catch (err) {
      console.log(err);
    } finally {
      setIsLoading(false);
    }
  };

  const refuseInvite = async () => {
    try {
      setIsLoading(true);
      const response = await fetchClient.get(`/api/clients/refuse-invite`);
      dispatch({
        type: GET_USER_REQUEST_SUCCESS,
        payload: response.data,
      });
      addToast(
        <div className="flex items-center p-2">
          <p className="text-xl">Invitation refused with success</p>
        </div>,
        {
          appearance: "success",
          autoDismiss: true,
        }
      );
    } catch (err) {
      console.log(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col w-full ">
      <h2 className="text-2xl text-grey-dark items-center pb-4 border-b border-grey-light mb-10 mt-6">
        Account details
      </h2>

      <div>
        <AccountForm account={user.account} />
      </div>

      <h2 className="text-2xl text-grey-dark items-center pb-4 border-b border-grey-light mb-10 mt-6">
        Invitation
      </h2>

      {user.inviteAccount ? (
        <div className="flex flex-row">
          <div className="flex flex-col justify-center">
            <strong className="text-blue">
              You've been invited to join an account
            </strong>
            <div className="text-grey-dark">
              If you accept you will change account, there is no going back.
              Please be certain.
            </div>
          </div>
          <Button
            className="ml-auto"
            isLoading={isLoading}
            onClick={acceptInvite}
          >
            Accept
          </Button>
          <Button
            className="ml-6"
            btnStyle="btn-outline"
            isLoading={isLoading}
            onClick={refuseInvite}
          >
            Refuse
          </Button>
        </div>
      ) : (
        <InviteForm />
      )}
    </div>
  );
};

export default Team;
