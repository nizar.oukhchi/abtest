import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from "react-toast-notifications";

import Button from "components/Button";
import Input from "components/Input";
import Select from "components/Select";
import AbTestLayout from "../AbTestLayout";

import { update } from "../../../api/abtest";
import { AB_TEST_UPDATE_VARIANT_SUCCESS } from "../../../redux/Abtests/types";

const ConversionView = () => {
  const { addToast } = useToasts();
  const dispatch = useDispatch();
  const currentTest = useSelector((state) => state.abtests.current);
  const [conversionPixels, setConversionPixels] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (currentTest.conversionPixels?.length > 0) {
      setConversionPixels(currentTest.conversionPixels);
    }
  }, [currentTest]);

  const conversionPixelsChangeHandler = (e, index) => {
    const { value, name } = e.target;

    const newConversionPixels = [...conversionPixels];
    newConversionPixels[index][name] = value;
    setConversionPixels(newConversionPixels);
  };

  const addConversionTest = () => {
    const newConversionPixels = [
      ...conversionPixels,
      {
        content: "",
        test: "equals",
      },
    ];

    setConversionPixels(newConversionPixels);
  };

  const removeConversionPixel = (index) => {
    const newConversionPixels = [...conversionPixels];
    newConversionPixels.splice(index, 1);
    setConversionPixels(newConversionPixels);
  };

  const saveConversionPixels = async () => {
    try {
      setIsLoading(true);
      const response = await update({
        ...currentTest,
        conversionPixels: conversionPixels,
      });
      dispatch({
        type: AB_TEST_UPDATE_VARIANT_SUCCESS,
        payload: response.data,
      });
      addToast(
        <div className="flex items-center p-2">
          <p className="text-xl">Conversion settings updated with success</p>
        </div>,
        {
          appearance: "success",
          autoDismiss: true,
        }
      );
    } catch (err) {
      console.log(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex-1">
      <h2 className="text-2xl text-grey-dark items-center pb-4 border-b border-grey-light mb-10">
        Conversion settings
      </h2>

      <div className="flex flex-col mb-10">
        {conversionPixels.map((item, index) => (
          <div className="flex items-center mb-6" key={index}>
            <Input
              type="text"
              label="url or text"
              name="content"
              value={item.content}
              className="flex-1 mr-8"
              onChange={(e) => conversionPixelsChangeHandler(e, index)}
            />
            <Select
              value={item.test}
              name="test"
              onChange={(e) => conversionPixelsChangeHandler(e, index)}
            >
              <option value="equals">Equals</option>
              <option value="contains">Contains</option>
            </Select>
            <Button
              btnStyle="btn-red"
              className="ml-8"
              onClick={() => removeConversionPixel(index)}
            >
              Delete
            </Button>
          </div>
        ))}
        <div className="flex mt-8">
          <Button type="button" className="ml-auto" onClick={addConversionTest}>
            Add conversion test
          </Button>

          <Button
            type="button"
            className="ml-8"
            btnStyle="btn-outline"
            onClick={saveConversionPixels}
            isLoading={isLoading}
          >
            Save
          </Button>
        </div>
      </div>
    </div>
  );
};

export default () => <AbTestLayout component={ConversionView} />;
