import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { fetchOneAction, updateAction } from 'redux/Abtests';

import AbTestLayoutComponent from './AbTestLayoutComponent';

const mapStateToProps = state => ({
    abTest: state.abtests.current,
    isLoading: state.abtests.isLoading
});

const mapDispatchToProps = dispatch => ({
    fetchOne: bindActionCreators(fetchOneAction, dispatch),
    updateTest: bindActionCreators(updateAction, dispatch)
});

const AbTestLayoutContainer = withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(AbTestLayoutComponent)
);

export default AbTestLayoutContainer;
