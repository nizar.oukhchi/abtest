import React, { useEffect } from 'react';

import AbTestHeader from '../AbTestHeader';

const AbTestLayoutComponent = ({
    component: Component,
    fetchOne,
    match,
    abtest,
    ...props
}) => {
    const { id } = match.params;

    useEffect(() => {
        fetchOne(id);
    }, []);

    return (
        <div className="flex flex-1 flex-col">
            <AbTestHeader />
            <Component {...props} />
        </div>
    );
};

export default AbTestLayoutComponent;
