import React from "react";
import { useSelector, useDispatch } from "react-redux";

import Button from "components/Button";
import { updateAction } from "redux/Abtests";

const AbTestHeader = () => {
    const abtest = useSelector(state => state.abtests.current);
    const isLoading = useSelector(state => state.abtests.isLoading);
    const dispatch = useDispatch();

    const enablePublish =
        abtest && abtest.variants && abtest.variants.length > 0;

    return (
        <div className="flex items-center h-16 mb-8">
            <h2 className="flex items-center text-base lg:text-xl text-grey-dark">
                <i className="typcn typcn-key-outline lg:text-2xl" />
                <span>{abtest.id}</span>
                <span
                    className={
                        (abtest.status === "LIVE"
                            ? "flag-green"
                            : "flag-orange") + " ml-4"
                    }
                >
                    {abtest.status}
                </span>
            </h2>

            {abtest.status === "DRAFT" && (
                <div className="flex items-center ml-auto">
                    {enablePublish ? (
                        <>
                            <div className="text-green-dark font-bold">
                                Ready to be executed
                            </div>
                            <Button
                                type="button"
                                isLoading={isLoading}
                                className="ml-8"
                                disabled={!enablePublish}
                                onClick={() => {
                                    dispatch(
                                        updateAction({
                                            id: abtest.id,
                                            status: "LIVE"
                                        })
                                    );
                                }}
                            >
                                Run
                            </Button>
                        </>
                    ) : (
                        <div className="text-orange font-bold">
                            Not ready yet to be executed
                        </div>
                    )}
                </div>
            )}
        </div>
    );
};

export default AbTestHeader;
