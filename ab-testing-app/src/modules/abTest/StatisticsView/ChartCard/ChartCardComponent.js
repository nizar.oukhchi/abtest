import React from 'react';
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  LabelList,
  Tooltip,
  Legend
} from 'recharts';

import Card from '../../../../components/Card';

const ChartCardComponent = ({ data }) => (
  <Card className="w-full p-8">
    <ResponsiveContainer width="100%" height={500}>
      <BarChart data={data}>
        <XAxis dataKey="name" tickLine={false} />
        <Tooltip />
        <Legend verticalAlign="top" height={50} />
        <Bar name="Conversion Rate (%)" dataKey="conversionRate" fill="#2991fc">
          <LabelList dataKey="conversionRate" position="top" />
        </Bar>
      </BarChart>
    </ResponsiveContainer>
  </Card>
);

export default ChartCardComponent;
