import React from "react";

import Card from "../../../components/Card";
import AbTestLayout from "../AbTestLayout";

import StatisticCard from "./StatisticCard";

const StatisticsComponent = ({ abTest, clientsList }) => {
    const visits =
        abTest &&
        abTest.variants &&
        abTest.variants.reduce((total, item) => {
            total = total + item.visits;
            return total;
        }, 0);
    const conversions =
        abTest &&
        abTest.variants &&
        abTest.variants.reduce((total, item) => {
            total = total + item.conversion;
            return total;
        }, 0);
    const conversionsRate = Math.floor((conversions / visits) * 100);
    const leadingVariant = abTest.leadingVariant;
    const variantsStatsData =
        abTest &&
        abTest.variants &&
        abTest.variants.map(item => {
            const { _id, name, conversionRate, visits, conversion } = item;
            return { _id, name, conversionRate, visits, conversion };
        });

    return (
        <div className="flex-1">
            <h2 className="text-2xl text-grey-dark items-center pb-4 border-b border-grey-light mb-10">
                Test general statistics
            </h2>
            <div className="flex flex-row justify-between mb-10">
                <div className="lg:w-1/5">
                    <StatisticCard
                        label={"visits"}
                        value={visits}
                        renderIcon={() => (
                            <i className="typcn typcn-group-outline" />
                        )}
                    />
                </div>
                <div className="lg:w-1/5">
                    <StatisticCard
                        label={"conversions"}
                        value={conversions}
                        renderIcon={() => (
                            <i className="typcn typcn-thumbs-up" />
                        )}
                    />
                </div>
                <div className="lg:w-1/5">
                    <StatisticCard
                        label={"conversions rate"}
                        value={conversionsRate}
                        unit={"%"}
                        renderIcon={() => (
                            <i className="typcn typcn-thermometer" />
                        )}
                    />
                </div>
                {leadingVariant && (
                    <div className="lg:w-1/5">
                        <StatisticCard
                            label={"Leading variant"}
                            value={leadingVariant.name}
                            renderIcon={() => (
                                <i className="typcn typcn-arrow-up-outline" />
                            )}
                        />
                    </div>
                )}
            </div>
            <h2 className="text-base lg:text-base text-grey-dark mb-6">
                Test variants statistics
            </h2>
            <div className="flex flex-row flex-wrap">
                {variantsStatsData &&
                    variantsStatsData.map((item, index) => (
                        <div key={index} className="lg:w-1/5 px-2">
                            <Card className="relative flex flex-col items-center w-full p-8">
                                {leadingVariant &&
                                    item._id === leadingVariant._id && (
                                        <span
                                            className="flag-green absolute"
                                            style={{ top: "15px" }}
                                        >
                                            <i className="typcn typcn-arrow-up-outline mr-1" />
                                            Leading
                                        </span>
                                    )}
                                <div className="text-base text-grey mb-6 mt-6">
                                    {item.name}
                                </div>
                                <div className="text-blue text-4xl uppercase mb-4">
                                    {item.conversionRate || "0"} %
                                </div>
                                <div className="text-base text-grey mb-2">
                                    {item.visits} visits
                                </div>
                                <div className="text-base text-grey">
                                    {item.conversion} converted
                                </div>
                            </Card>
                        </div>
                    ))}
            </div>
        </div>
    );
};

export default () => <AbTestLayout component={StatisticsComponent} />;
