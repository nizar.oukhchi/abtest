import React from 'react';

import Card from '../../../../components/Card';

const StatisticCardComponent = ({ label, value, unit, renderIcon }) => (
  <Card>
    <div className="flex flex-col text-blue items-center p-4">
      <div className="text-4xl mb-2">{renderIcon()}</div>
      <h2 className="flex items-center mb-4" style={{ height: '60px' }}>
        {typeof value === 'string' && (
          <span className="text-xl uppercase">{value}</span>
        )}

        {isNaN(value) &&
          typeof value !== 'string' && (
          <span className="text-5xl uppercase">-</span>
        )}

        {typeof value === 'number' &&
          !isNaN(value) && (
          <span className="text-5xl font-bold uppercase">
            {' '}
            {value}
            {unit}
          </span>
        )}
      </h2>
      <p className="text-base text-grey">{label}</p>
    </div>
  </Card>
);

export default StatisticCardComponent;
