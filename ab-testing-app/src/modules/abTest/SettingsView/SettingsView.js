import React from "react";
import { useSelector, useDispatch } from "react-redux";

import { ModalContext } from "components/Modal";
import Button from "components/Button";
import AbTestLayout from "../AbTestLayout";
import { createLoadingSelector } from "redux/Loading";
import { removeAction } from "redux/Abtests";

const ScriptClientModalContent = ({ clientKey }) => (
  <div className="w-full">
    <h2 className="text-xl text-grey-darker">
      Copy and past the code below in your tag manager or directly past it in
      the head of the page
    </h2>
    <pre className="shadow-inner bg-grey-lightest border border-grey-lighter rounded p-2 mt-6">
      {`  <script>
    const intervalId = setInterval(function() {
      if (window.AlgorithmClient) {
        new AlgorithmClient('${clientKey}').init();
        clearInterval(intervalId);
      }
    }, 100);
  </script>
  <script type="text/javascript" src="https://unpkg.com/ab-testing-client/lib/ab-testing-client.min.js"></script>`}
    </pre>
  </div>
);

const SettingsView = ({ abTest }) => {
  const { handleModal } = React.useContext(ModalContext);

  const isRemoveLoading = useSelector((state) =>
    createLoadingSelector(["ab_test_remove"])(state)
  );
  const dispatch = useDispatch();
  
  return (
    <div className="flex-1">
      <h2 className="text-2xl text-grey-dark items-center pb-4 border-b border-grey-light mb-10">
        Settings
      </h2>

      <div className="flex flex-row mb-10">
        <div className="flex flex-col justify-center">
          <strong className="text-blue">Client script configuration</strong>
          <div className="text-grey-dark">
            You need to configure the client script in order for the test to
            work
          </div>
        </div>
        <Button
          className="ml-auto"
          btnStyle="btn"
          isLoading={isRemoveLoading}
          onClick={() =>
            handleModal(
              <ScriptClientModalContent clientKey={abTest.creator?.clientKey} />
            )
          }
        >
          Show client script
        </Button>
      </div>

      <div className="flex flex-row">
        <div className="flex flex-col justify-center">
          <strong className="text-red">Delete this Test</strong>
          <div className="text-grey-dark">
            Once you delete a repository, there is no going back. Please be
            certain.
          </div>
        </div>
        <Button
          className="ml-auto"
          btnStyle="btn-red"
          isLoading={isRemoveLoading}
          onClick={() => dispatch(removeAction(abTest.id))}
        >
          Delete this Test
        </Button>
      </div>
    </div>
  );
};

export default () => <AbTestLayout component={SettingsView} />;
