import React, { useState, useEffect } from "react";
import { Redirect } from "react-router";
import { useToasts } from "react-toast-notifications";

import Input from "components/Input";
import Button from "components/Button";
import AbTestLayout from "../AbTestLayout";

const DetailsComponent = ({ abTest, updateTest, isLoading }) => {
  const [temporaryAbtest, seTemporaryAbtest] = useState(abTest);
  const { addToast } = useToasts();

  useEffect(() => {
    seTemporaryAbtest(abTest);
  }, [abTest]);

  const updateInputValue = (e) => {
    seTemporaryAbtest({
      ...temporaryAbtest,
      [e.target.name]: e.target.value,
    });
  };

  return abTest && abTest.id ? (
    <div className="flex-1">
      <h2 className="text-2xl text-grey-dark items-center pb-4 border-b border-grey-light mb-10">
        Test details
      </h2>
      <form
        className="flex flex-1 flex-col lg:flex-col"
        onSubmit={(e) => {
          e.preventDefault();
          updateTest({
            id: temporaryAbtest.id,
            name: temporaryAbtest.name,
            url: temporaryAbtest.url,
            description: temporaryAbtest.description,
            nodeId: temporaryAbtest.nodeId,
            conversionButtonSelector: temporaryAbtest.conversionButtonSelector,
          });

          addToast(
            <div className="flex items-center p-2">
              <p className="text-xl">Test details updated with success</p>
            </div>,
            {
              appearance: "success",
              autoDismiss: true,
            }
          );
        }}
      >
        <Input
          label="Ab test name"
          className="mt-2"
          type="text"
          name="name"
          value={temporaryAbtest.name}
          onChange={updateInputValue}
        />
        <Input
          label="Website url"
          className="mt-4"
          type="text"
          name="url"
          value={temporaryAbtest.url}
          onChange={updateInputValue}
        />

        <Input
          label="Conversion button selector"
          className="mt-4"
          type="text"
          name="conversionButtonSelector"
          value={temporaryAbtest.conversionButtonSelector}
          onChange={updateInputValue}
        />

        <Input
          label="Description"
          className="mt-4"
          type="text"
          name="description"
          value={temporaryAbtest.description}
          onChange={updateInputValue}
          inputType="textarea"
        />
        <div className="flex mt-8">
          <Button
            type="submit"
            className="ml-auto"
            isLoading={isLoading}
            btnStyle="btn-outline"
          >
            Save
          </Button>
        </div>
      </form>
      <div className="flex-1" />
    </div>
  ) : (
    <Redirect to="/" />
  );
};

export default () => <AbTestLayout component={DetailsComponent} />;
