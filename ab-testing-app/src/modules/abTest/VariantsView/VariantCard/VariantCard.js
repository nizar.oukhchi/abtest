import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import Card from "components/Card";
import Input from "components/Input";
import Button from "components/Button";

import { updateVariantNameAction } from "redux/Abtests/actions";
import { createLoadingSelector } from "redux/Loading";

const VariantCard = ({ variant, update, preview }) => {
  const dispatch = useDispatch();
  const isUpdateLoading = useSelector((state) =>
    createLoadingSelector(["ab_test_update_variant"])(state)
  );
  const [name, setName] = useState(variant.name);
  const [isEditMode, setIsEditMode] = useState(false);

  useEffect(() => {
    if (isEditMode && !isUpdateLoading) setIsEditMode(false);
  }, [isUpdateLoading]);

  return (
    <Card className="flex flex-col relative" style={{ height: 220 }}>
      <div className="card-content relative">
        <div className="flex content-center">
          <a className="card-title truncate">{variant.name}</a>
          <a
            className="text-xl text-grey-darker cursor-pointer no-underline ml-auto"
            onClick={() => {
              setIsEditMode(true);
            }}
          >
            <i className="typcn typcn-edit" />
          </a>
        </div>

        <a className="card-subtitle mt-3">
          <i className="text-base typcn typcn-group-outline mr-1" />
          {variant.visits} visits so far
        </a>
        <a className="card-subtitle mt-1">
          <i className="text-base typcn typcn-thumbs-up mr-1" />
          {variant.conversion} conversions
        </a>
        <a className="card-subtitle mt-1">
          <i className="text-base typcn typcn-thermometer mr-1" />
          {isNaN(variant.conversionRate) ? "-" : variant.conversionRate} %
          conversion rate
        </a>
      </div>
      <div className="flex mt-auto pr-2 pb-2">
        <Button
          btnStyle="btn-outline"
          className="ml-auto"
          onClick={() => {
            preview(variant.id);
          }}
        >
          Preview
        </Button>
      </div>
      {isEditMode && (
        <div className="absolute inset-0 bg-white">
          <div>
            <div className="p-5">
              <Input
                label="Variant name"
                value={name}
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
              <div className="flex flex-col mt-4">
                <Button
                  isLoading={isUpdateLoading}
                  onClick={() => {
                    dispatch(updateVariantNameAction({ id: variant.id, name }));
                  }}
                >
                  Save
                </Button>
                <Button
                  btnStyle="btn-outline"
                  className="mt-2"
                  onClick={() => {
                    setIsEditMode(false);
                  }}
                >
                  CLose
                </Button>
              </div>
            </div>
          </div>
        </div>
      )}
    </Card>
  );
};

export default VariantCard;
