import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import Button from "components/Button";
import SvgElement from "components/SvgElement";
import AbTestLayout from "../AbTestLayout";
import { createLoadingSelector } from "redux/Loading";
import { generateVariantsAction } from "redux/Abtests";

import VariantCard from "./VariantCard";

const VariantsComponent = ({ abTest, updateTest, isLoading }) => {
  const [variantList, setVariantList] = useState(abTest.variants || []);
  const isGenerationLoading = useSelector((state) =>
    createLoadingSelector(["ab_test_generate_variants"])(state)
  );
  const dispatch = useDispatch();

  useEffect(() => {
    const _variantList = abTest.variants || [];
    setVariantList(_variantList);
  }, [abTest]);

  const openVariantPreview = (id) => {
    const url = new URL(`http://${abTest.url}`);
    url.searchParams.append("bandit-preview", id);
    window.open(url, "_blank");
  };

  return (
    <div className="flex-1">
      {abTest.domElementsCount === 0 ? (
        <div
          className="flex flex-col items-center justify-center mx-auto"
          style={{ width: "450px", height: "400px" }}
        >
          <SvgElement ilustrationName="elements" />

          <span className="text-xl text-grey leading-normal text-center mt-4">
            There is no element associated with this this yet, open the editor
            using the button bellow
          </span>

          <Button
            type="button"
            className="mt-4"
            onClick={() => {
              const url = `http://${abTest.url}?thompson-test-id=${abTest.id}`;
              window.open(url, "_blank");
            }}
          >
            open editor
          </Button>
        </div>
      ) : abTest.variantsGenerated ? (
        <>
          <h2 className="text-2xl text-grey-dark items-center pb-4 border-b border-grey-light mb-10">
            Test variants
          </h2>

          <div className="flex flex-1 flex-wrap">
            {abTest.variants.map((item) => (
              <div key={item.id} className="lg:w-1/4 px-2">
                <VariantCard variant={item} preview={openVariantPreview}/>
              </div>
            ))}
          </div>
        </>
      ) : (
        <div
          className="flex flex-col items-center justify-center mx-auto"
          style={{ width: "450px", height: "400px" }}
        >
          <SvgElement ilustrationName="factory" />

          <span className="text-xl text-grey leading-normal text-center mt-6 font-bold">
            You have {abTest.domElementsCount} dom elements variants in your
            test.
          </span>

          <span className="text-xl text-grey leading-normal text-center mt-4">
            When you're ready, click on the button bellow to generate the test
            variants
          </span>

          <Button
            type="button"
            className="mt-4"
            isLoading={isGenerationLoading || abTest.processing}
            onClick={() => {
              dispatch(generateVariantsAction(abTest.id));
            }}
          >
            Generate variants
          </Button>
        </div>
      )}
    </div>
  );
};

export default () => <AbTestLayout component={VariantsComponent} />;
