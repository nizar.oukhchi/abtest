import React, { useState } from "react";
import { Redirect, NavLink } from "react-router-dom";
import GoogleLogin from "react-google-login";
import { useDispatch, useSelector } from "react-redux";

import { authenticateAction } from "redux/Auth/actions";
import { createLoadingSelector } from "../../redux/Loading";

import Button from "../../components/Button";
import Input from "../../components/Input";

const Signup = ({}) => {
  const isAuthenticated = useSelector((state) => !!state.auth.token);
  const error = useSelector((state) => state.auth.error);
  const isLoading = useSelector((state) =>
    createLoadingSelector(["authenticate"])(state)
  );
  const dispatch = useDispatch();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [name, setName] = useState();
  const [accountName, setAccountName] = useState();

  const responseGoogle = (response) => {
    const { email, googleId, name } = response.profileObj;
    dispatch(authenticateAction({ email, googleId, name }, "google"));
  };

  return !isAuthenticated ? (
    <div className="flex flex-col h-full">
      <div className="flex items-center mb-6">
        <a>Logo</a>
        <NavLink to="/login" className="ml-auto">
          <Button btnStyle="btn-outline">Already have an account ?</Button>
        </NavLink>
      </div>
      <div className="flex flex-col lg:w-2/3 m-auto">
        <h2 className="text-4xl mb-2">Signup</h2>
        <p className="mb-6">Create a new account</p>
        {error && <p className="text-red mb-2">{error}</p>}
        <form
          onSubmit={(e) => {
            e.preventDefault();
            dispatch(authenticateAction({ email, password, name, accountName }, "signup"));
          }}
        >
          <Input
            label="Account name"
            type="text"
            name="accountName"
            value={accountName}
            onChange={(e) => setAccountName(e.target.value)}
          />
          <Input
            label="Display name"
            type="text"
            name="name"
            className="mt-4"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <Input
            label="Email Address"
            type="email"
            className="mt-4"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Input
            label="Password"
            type="password"
            className="mt-4"
            name="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <div className="flex items-center justify-center mt-8">
            <Button type="submit" isLoading={isLoading}>
              Create an account
            </Button>
            <span className="mx-4">Or</span>
            <GoogleLogin
              type="button"
              clientId="949466791654-c5fe5or94jleh4qh7092ukssdi193fi9.apps.googleusercontent.com"
              buttonText="Continue with google"
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              cookiePolicy={"single_host_origin"}
            />
          </div>
          <div className="flex items-center mt-8">
            <a className="text-sm text-blue-dark">Forgot password</a>
          </div>
        </form>
      </div>
      <div className="flex items-center mt-10">
        <a className="text-sm">Copyright 2020 Company name</a>
      </div>
    </div>
  ) : (
    <Redirect to="/" />
  );
};

export default Signup;
