import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { authenticateAction } from 'redux/Auth/actions';
import { createLoadingSelector } from "../../redux/Loading";

import LoginComponent from './LoginComponent';


const loadingSelector = createLoadingSelector(["authenticate"]);

const mapStateToProps = state => ({
    isLoading: loadingSelector(state),
    isAuthenticated: !!state.auth.token,
    error: state.auth.error
});

const mapDispatchToProps = dispatch => ({
    authenticate: bindActionCreators(authenticateAction, dispatch)
});

const LoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginComponent);

export default LoginContainer;
