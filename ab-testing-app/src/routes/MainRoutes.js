import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";

import AuthLayout from "../components/AuthLayout";
import MainLayout from "../components/MainLayout";
import Login from "../modules/login";
import Signup from "../modules/Signup";
import Index from "../modules/index";
import AbTestDetails from "../modules/abTest/DetailsView";
import AbTestVariants from "../modules/abTest/VariantsView";
import AbTestStatistics from "../modules/abTest/StatisticsView";
import AbTestSettings from "../modules/abTest/SettingsView";
import AbTestConversionView from "../modules/abTest/ConversionView";
import Profile from "../modules/Profile";

export default (
  <BrowserRouter>
    <Switch>
      <AuthLayout exact path="/login" component={Login} />
      <AuthLayout exact path="/signup" component={Signup} />
      <MainLayout exact path="/" component={Index} />
      <MainLayout exact path="/abtest/:id/details" component={AbTestDetails} />
      <MainLayout
        exact
        path="/abtest/:id/variants"
        component={AbTestVariants}
      />
      <MainLayout
        exact
        path="/abtest/:id/conversion"
        component={AbTestConversionView}
      />
      <MainLayout
        exact
        path="/abtest/:id/statistics"
        component={AbTestStatistics}
      />
      <MainLayout
        exact
        path="/abtest/:id/settings"
        component={AbTestSettings}
      />
      <MainLayout path="/profile" component={Profile} />
    </Switch>
  </BrowserRouter>
);
