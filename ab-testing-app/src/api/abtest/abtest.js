import fetchClient from "utils/fetchClient";

const ABTEST_API = "/api/thompsons";

export const create = (data) => {
  return fetchClient.post(ABTEST_API, data);
};

export const fetchAll = () => {
  return fetchClient.get(ABTEST_API);
};

export const fetchOne = (id) => {
  return fetchClient.get(`${ABTEST_API}/${id}`);
};

export const update = (item) => {
  return fetchClient.put(`${ABTEST_API}/${item.id}`, item);
};

export const remove = (id) => {
  return fetchClient.delete(`${ABTEST_API}/${id}`);
};

export const generateVariants = (id) => {
  return fetchClient.get(`${ABTEST_API}/generate/${id}`);
};

export const updateVariantName = ({ id, name }) => {
  return fetchClient.put(`${ABTEST_API}/variant/${id}`, { name });
};
