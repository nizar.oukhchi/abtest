import fetchClient from "utils/fetchClient";

export const authenticate = (values, type) =>
  fetchClient.post(`/api/auth/${type}`, values);

export const getCurrentUser = () => fetchClient.get("/api/auth/me");
