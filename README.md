# AB Test solution - proof of concept

This is an a proof of concept of an A/B testing platform.

This project contains the backend api and the administration console.

## Prerequisites

Before running the app you need to ensure having those prerequisites:

Nodejs 8+

Mongodb: mongodb must be running in port 27017

## Getting started

```sh
cd ab-testing-app
```

install dependencies

```sh
yarn
```

Run the app in dev mode

```sh
yarn dev
```

## Tech stack

The application uses some of the latest innovation in the web, it is based on a modern Nodejs and React stack. it implements a server side rendering capabilities powered by NextJS and realtime communication powered by socket.IO

Client saide it uses React, Redux and Tailwindcss, an unopinionated and utility-first CSS framework for rapidly building custom user interfaces.

The complete tech stack:

- React
- Redux
- Express
- Socket.io
- Mongodb
- TailwindCss

## Architecture

Folder structure

![](folder-structure.png)

Architecture

![](architecture.png)

## Configuration

The .env file contains the necessary configuration variables.

We will adapt the the file to different type of env (DEV, STAGGING, PROD)

## How to test the staging env.

Backend Side:

- Create an abtest with the url of the website you want to test and make sure the NODE ID and CONVERSION BUTTON ID are valide dom element in the target website

- Create some test variantes

- Publish the abtest

Client Side (website target):

- Add the ab-testing-client.js file from ab-testing-client/lib to the end of the body of the page (or use the cdn file like in the example below)

- Add a script tag after the ab-testing-client.js script like this (make sure the put the right CLIENT-ID value, you'll find it in the backoffice app when you open the side menu):

```javascript
    <script>
      const intervalId = setInterval(function() {
        if (window.AbTestingClient) {
          new AbTestingClient('CLIENT-ID-XXXXXXXXX').init(),
            clearInterval(intervalId);
        }
      }, 100);
    </script>
    <script
      async
      src="https://cdn.jsdelivr.net/npm/ab-testing-client@1.1.1/lib/ab-testing-client.min.js"
    ></script>
```

- that's it
