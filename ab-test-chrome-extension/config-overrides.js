const { override, addBabelPreset } = require('customize-cra');

const buildFileName = 'production';

function myOverride(config, env) {
  // Change the JS output file name and path, from 'static/js/[name].[contenthash:8].js' to `static/${buildFileName}.js`
  config.output = {
    ...config.output,
    filename: `static/js/${buildFileName}.js`,
    chunkFilename: `static/js/[name].${buildFileName}.chunk.js`
  };

  // Change the CSS output file name and path, from 'static/css/[name].[contenthash:8].css' to `static/${buildFileName}.css`
  config.plugins.map((plugin, i) => {
    if (
      plugin.options &&
      plugin.options.filename &&
      plugin.options.filename.includes('static/css')
    ) {
      config.plugins[i].options.filename = `static/css/${buildFileName}.css`;
      config.plugins[
        i
      ].options.chunkFilename = `static/css/${buildFileName}.chunk.css`;
    }
  });

  console.log('Additional config was applied through config-overrides.js');

  return config;
}

module.exports = override(
  myOverride,
  addBabelPreset('@emotion/babel-preset-css-prop')
);
