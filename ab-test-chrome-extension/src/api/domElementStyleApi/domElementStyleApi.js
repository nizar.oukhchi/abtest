import fetchClient from '../../utils/fetchClient';

export const getAllBySelector = ({ selector, abtest }) =>
  fetchClient.post(`/api/dom-element-styles/getAll`, { selector, abtest });

export const createOrUpdate = ({ uid, ...data }, originalElementDataStyle) => {
  const filteredStyleData = {};
  for (let [key, value] of Object.entries(data.style)) {
    if (value !== originalElementDataStyle[key]) {
      filteredStyleData[key] = value;
    }
  }

  return fetchClient.post(`/api/dom-element-styles/${uid}`, {
    ...data,
    style: filteredStyleData,
  });
};

export const updateMany = (data) => {
  return fetchClient.put(`/api/dom-element-styles`, data);
};

export const deleteOne = (uid) =>
  fetchClient.delete(`/api/dom-element-styles/${uid}`);

export const deleteMany = (thompson) =>
  fetchClient.delete(`/api/dom-element-styles?thompson=${thompson}`);
