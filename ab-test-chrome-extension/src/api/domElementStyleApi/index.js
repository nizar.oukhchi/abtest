export {
  createOrUpdate,
  getAllBySelector,
  deleteOne,
  deleteMany,
  updateMany
} from './domElementStyleApi';
