/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react';
import { css } from '@emotion/core';
import {
  Button,
  Text,
  IconSave,
  IconEye,
  IconPlus,
  CloseButton,
  InputGroup,
  Input,
  IconCheckCircle,
  IconCircle,
  Spinner,
  IconTrash2,
  IconX,
} from 'sancho';
import Draggable from 'react-draggable';
import uuidv4 from 'uuid/v4';
import { ToastContainer, toast } from 'react-toastify';
import Switch from 'react-switch';

import HtmlEditor from './components/HtmlEditor';
import Highlighter from './components/Highlighter';

import findSelector from './utils/selectorFinder';
import applyStyleOnDomElement from './utils/applyStyleOnDomElement';

import {
  createOrUpdate,
  getAllBySelector,
  deleteOne,
  deleteMany,
  updateMany,
} from './api/domElementStyleApi';

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

const THOMPSON =
  urlParams.get('thompson-test-id') || '5ebb431295cef4608ce97448';

const App = () => {
  const [highlighterPosition, setHightlighterPosition] = useState({});
  const [selectorPosition, setSelectorPosition] = useState({});

  const [elementVariants, setElementVariants] = useState([]);
  const [currentElementVariant, setCurrentElementVariant] = useState({
    style: {},
    content: '',
    src: '',
    nodeType: '',
  });

  const [highlightedElement, setHighlightedElement] = useState({
    nodeName: '',
  });
  const [selectedElement, setSelectedElement] = useState();
  const [selector, setSelector] = useState('');
  const [isLoadingElements, setIsLoadingElements] = useState(false);
  const [isSavingElement, setIsSavingElement] = useState(false);
  const [isDeletingElement, setIsDeletingElement] = useState(false);
  const [originalElementDataStyle, setOriginalElementDataStyle] = useState({});

  useEffect(() => {
    document.addEventListener('mouseover', elemenMouseoverHandler);
    document.addEventListener('mouseout', elemenMouseoutHandler);

    return () => {
      document.removeEventListener('mouseover', elemenMouseoverHandler);
      document.removeEventListener('mouseout', elemenMouseoutHandler);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [highlighterPosition]);

  useEffect(() => {
    document.addEventListener('click', elementClickHandler, true);

    return () => {
      document.removeEventListener('click', elementClickHandler);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const elementClickHandler = async (e) => {
    if (
      e.target === document ||
      e.target === document.body ||
      e.target === document.documentElement ||
      e.path[0].id === 'extension-wrapper'
    ) {
      return;
    }
    e.preventDefault();
    e.stopPropagation();
    elementSelected(e.target);
  };

  const elemenMouseoverHandler = (e) => {
    if (
      e.target === document ||
      e.target === document.body ||
      e.target === document.documentElement ||
      e.path[0].id === 'extension-wrapper'
    ) {
      return;
    }

    const { left, top, width, height } = e.target.getBoundingClientRect();
    const scrollLeft =
      window.pageXOffset || document.documentElement.scrollLeft;
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    setHighlightedElement(e.target);
    setHightlighterPosition({
      left: left + scrollLeft,
      top: top + scrollTop,
      width,
      height,
      display: 'block',
    });
  };

  const elemenMouseoutHandler = (e) => {
    if (
      e.target === document ||
      e.target === document.body ||
      e.target === document.documentElement ||
      e.path[0].id === 'extension-wrapper'
    )
      return;

    const { left, top, width, height } = e.target.getBoundingClientRect();
    setHightlighterPosition({
      left,
      top,
      width,
      height,
      display: 'none',
    });
  };

  const elementSelected = async (selectedDomElement) => {
    setIsLoadingElements(true);

    const _selector = findSelector(selectedDomElement);

    setSelector(_selector);

    setSelectedElement(selectedDomElement);

    const {
      left,
      top,
      width,
      height,
    } = selectedDomElement.getBoundingClientRect();
    const scrollLeft =
      window.pageXOffset || document.documentElement.scrollLeft;
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    setSelectorPosition({
      left: left + scrollLeft,
      top: top + scrollTop,
      width,
      height,
      display: 'block',
    });

    try {
      const { data } = await getAllBySelector({
        selector: [
          findSelector(selectedDomElement, false),
          findSelector(selectedDomElement, true),
        ],
        abtest: THOMPSON,
      });
      const elements = data;

      setIsLoadingElements(false);

      const targetComputedStyle = getComputedStyle(selectedDomElement);
      const firstElementVariant = {
        uid: uuidv4(),
        abtest: THOMPSON,
        selector: _selector,
        useForConversion: false,
        isMultiple: false,
        content: selectedDomElement.innerHTML,
        src: selectedDomElement.src,
        style: {
          width: targetComputedStyle.width,
          height: targetComputedStyle.height,
          top: targetComputedStyle.top,
          bottom: targetComputedStyle.bottom,
          left: targetComputedStyle.left,
          right: targetComputedStyle.right,
          fontFamily: targetComputedStyle.fontFamily,
          fontSize: targetComputedStyle.fontSize,
          fontWeight: targetComputedStyle.fontWeight,
          textAlign: targetComputedStyle.textAlign,
          color: targetComputedStyle.color,
          fontStyle: targetComputedStyle.fontStyle,
          backgroundColor: targetComputedStyle.backgroundColor,
          backgroundPosition: targetComputedStyle.backgroundPosition,
          backgroundRepeat: targetComputedStyle.backgroundRepeat,
          backgroundSize: targetComputedStyle.backgroundSize,
        },
        nodeType: selectedDomElement.nodeName.toLowerCase(),
      };

      if (elements.length) {
        const style = { ...firstElementVariant.style, ...elements[0].style };
        setOriginalElementDataStyle({ ...firstElementVariant.style });
        setElementVariants(elements.filter((e, i) => i < 2));
        setCurrentElementVariant({ ...elements[0], style });
        applyStyleOnDomElement(selectedDomElement, elements[0]);
      } else {
        setOriginalElementDataStyle({ ...firstElementVariant.style });
        setElementVariants([firstElementVariant]);
        setCurrentElementVariant({ ...firstElementVariant });
      }
    } catch (err) {
      console.log(err);
    }
  };

  const unselectElement = () => {
    setSelectedElement(null);
    setSelectorPosition({
      display: 'none',
    });
  };

  const updateStyleProperty = (e) => {
    setCurrentElementVariant({
      ...currentElementVariant,
      style: {
        ...currentElementVariant.style,
        [e.target.name]: e.target.value,
      },
    });
  };

  const applyStyle = () => {
    applyStyleOnDomElement(selectedElement, currentElementVariant);
  };

  const updateContent = (code) => {
    setCurrentElementVariant({
      ...currentElementVariant,
      content: code,
    });
  };

  const updateImageSrc = (e) => {
    setCurrentElementVariant({
      ...currentElementVariant,
      src: e.target.value,
    });
  };

  const saveElement = async () => {
    try {
      setIsSavingElement(true);
      const response = await createOrUpdate(
        currentElementVariant,
        originalElementDataStyle
      );
      setIsSavingElement(false);
      const savedElement = response.data;
      const tempElementVariants = [...elementVariants];
      const foundIndex = tempElementVariants.findIndex(
        (x) => x.uid === savedElement.uid
      );
      tempElementVariants[foundIndex] = savedElement;
      const style = { ...originalElementDataStyle, ...savedElement.style };
      setCurrentElementVariant({ ...savedElement, style });
      setElementVariants(tempElementVariants);
      applyStyleOnDomElement(selectedElement, savedElement);
      toast('Variant saved succefully !');
    } catch (err) {
      setIsSavingElement(false);
      console.log(err);
    }
  };

  const addVariant = async () => {
    setIsSavingElement(true);
    const response = await createOrUpdate(
      currentElementVariant,
      originalElementDataStyle
    );
    setIsSavingElement(false);
    const savedElement = response.data;
    const tempElementVariants = [...elementVariants];
    const foundIndex = tempElementVariants.findIndex(
      (x) => x.uid === savedElement.uid
    );
    tempElementVariants[foundIndex] = savedElement;

    const newElementVariant = {
      uid: uuidv4(),
      abtest: THOMPSON,
      selector,
      isMultiple: currentElementVariant.isMultiple,
      content: selectedElement.innerHTML,
      src: selectedElement.src,
      style: originalElementDataStyle,
      nodeType: selectedElement.nodeName.toLowerCase(),
    };

    setElementVariants([...tempElementVariants, newElementVariant]);
    setCurrentElementVariant({ ...newElementVariant });

    applyStyleOnDomElement(selectedElement, newElementVariant);
  };

  const selectDomElementVariant = (uid) => {
    const el = elementVariants.find((i) => i.uid === uid);
    const style = { ...originalElementDataStyle, ...el.style };
    setCurrentElementVariant({ ...el, style });
    applyStyleOnDomElement(selectedElement, { ...el, style });
  };

  const deleteElement = async () => {
    try {
      setIsDeletingElement(true);
      await deleteOne(currentElementVariant.uid);
      setIsDeletingElement(false);
      if (elementVariants.length === 1) {
        unselectElement();
        applyStyleOnDomElement(selectedElement, {
          content: selectedElement.innerHTML,
          src: selectedElement.src,
          style: originalElementDataStyle,
          nodeType: selectedElement.nodeName.toLowerCase(),
        });
      } else {
        const newElementsVariants = elementVariants.filter(
          (i) => i.uid !== currentElementVariant.uid
        );
        setElementVariants(newElementsVariants);
        const style = {
          ...originalElementDataStyle,
          ...newElementsVariants.style,
        };
        setCurrentElementVariant({ ...newElementsVariants[0], style });
        applyStyleOnDomElement(selectedElement, {
          ...newElementsVariants[0],
          style,
        });
      }
    } catch (err) {
      console.log(err);
      setIsDeletingElement(false);
    }
  };

  const deleteAllElements = async () => {
    unselectElement();
    try {
      setIsDeletingElement(true);
      await deleteMany(THOMPSON);
      setIsDeletingElement(false);
    } catch (err) {
      console.log(err);
      setIsDeletingElement(false);
    }
  };

  const changeSelectorMode = async (checked) => {
    const newSelector = findSelector(selectedElement, checked);
    setSelector(newSelector);
    setCurrentElementVariant({
      ...currentElementVariant,
      isMultiple: checked,
      selector: newSelector,
    });

    const { data } = await updateMany({
      selector: currentElementVariant.selector,
      newSelector,
      abtest: THOMPSON,
      isMultiple: checked,
    });

    if (data.length > 0) {
      setElementVariants(data);
    }
  };

  const changeUseForConversion = async (checked) => {
    setCurrentElementVariant({
      ...currentElementVariant,
      useForConversion: checked,
    });
    const { data } = await updateMany({
      selector: currentElementVariant.selector,
      abtest: THOMPSON,
      useForConversion: checked,
    });

    if (data.length > 0) {
      setElementVariants(data);
    }
  };

  return (
    <>
      <Highlighter
        position={highlighterPosition}
        name={highlightedElement.nodeName.toLocaleLowerCase()}
        color="#33a5fc"
        zIndex="99999999998"
      />
      <Highlighter
        position={selectorPosition}
        name={currentElementVariant.nodeType.toLocaleLowerCase()}
        color="#5c62ed"
        zIndex="99999999999"
      />

      {selectedElement && (
        <Draggable
          handle=".handle"
          defaultPosition={{ x: 0, y: 0 }}
          position={null}
          grid={[25, 25]}
          scale={1}
        >
          <div
            css={css`
              position: fixed;
              overflow: hidden;
              background-color: white;
              margin-bottom: 60px;
              border-radius: 5px;
              box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12),
                0 1px 2px rgba(0, 0, 0, 0.24);
              z-index: 9999999999;
              pointer-events: all;
            `}
            style={{
              width: '450px',
              height: '500px',
              right: `10px`,
              bottom: `10px`,
            }}
          >
            <div
              css={css`
                display: flex;
                flex-direction: column;
                height: 100%;
              `}
            >
              <div
                className="handle"
                css={css`
                  display: flex;
                  align-items: center;
                  background-image: linear-gradient(
                    to right bottom,
                    #33a5fc,
                    #2991fc,
                    #3e7bf7,
                    #5c62ed,
                    #7841dc
                  );
                  cursor: move;
                  height: 50px;
                `}
              >
                <Text variant="h5" gutter={false}>
                  <div
                    css={css`
                      width: 360px;
                      color: white;
                      margin-left: 5px;
                      white-space: nowrap;
                      overflow: hidden;
                      text-overflow: ellipsis;
                    `}
                  >
                    {currentElementVariant.selector}
                  </div>
                </Text>

                <CloseButton
                  css={css`
                    margin-left: auto;
                    color: white;
                    margin-right: 10px;
                    cursor: pointer;
                  `}
                  color="white"
                  onPress={unselectElement}
                />
              </div>
              <div
                css={css`
                  display: flex;
                  flex: 1;
                  flex-direction: column;
                  overflow-y: scroll;
                  padding: 16px;
                `}
              >
                <div
                  css={css`
                    display: flex;
                    margin-bottom: 30px;
                    flex-direction: column;
                  `}
                >
                  <label
                    css={css`
                      display: flex;
                      align-items: center;
                      margin-bottom: 20px;
                    `}
                  >
                    <Text
                      variant="paragraph"
                      css={css`
                        margin-right: 10px;
                        margin-bottom: 0px;
                        color: #1971c2;
                        font-weight: bold;
                      `}
                    >
                      Multiple elements mode
                    </Text>

                    <Switch
                      onChange={changeSelectorMode}
                      checked={currentElementVariant.isMultiple}
                      checkedIcon={false}
                      uncheckedIcon={false}
                    />
                  </label>
                  {(currentElementVariant.nodeType === 'form' ||
                    currentElementVariant.nodeType === 'button' ||
                    currentElementVariant.nodeType === 'a') && (
                    <label
                      css={css`
                        display: flex;
                        align-items: center;
                      `}
                    >
                      <Text
                        variant="paragraph"
                        css={css`
                          margin-right: 10px;
                          margin-bottom: 0px;
                          color: #1971c2;
                          font-weight: bold;
                        `}
                      >
                        {`Use this <${currentElementVariant.nodeType}> for conversion`}
                      </Text>

                      <Switch
                        onChange={changeUseForConversion}
                        checked={currentElementVariant.useForConversion}
                        checkedIcon={false}
                        uncheckedIcon={false}
                      />
                    </label>
                  )}
                </div>
                <div
                  css={css`
                    display: flex;
                    flex-wrap: wrap;
                  `}
                >
                  {elementVariants.map((e, i) => (
                    // eslint-disable-next-line jsx-a11y/anchor-is-valid
                    <Button
                      intent="primary"
                      variant="ghost"
                      className="___add_variant_btn"
                      iconBefore={
                        currentElementVariant.uid === e.uid ? (
                          <IconCheckCircle />
                        ) : (
                          <IconCircle />
                        )
                      }
                      css={css`
                        max-width: 130px;
                        color: #2b6cb0;
                        margin-right: 8px;
                        cursor: pointer;
                      `}
                      key={e.uid}
                      onClick={() => {
                        selectDomElementVariant(e.uid);
                      }}
                    >
                      {`variant ${i + 1} ${e.id ? '' : '*'}`}
                    </Button>
                  ))}

                  <Button
                    intent="primary"
                    variant="ghost"
                    iconBefore={<IconPlus />}
                    className="___add_variant_btn"
                    css={css`
                      max-width: 130px;
                      color: #2b6cb0;
                      margin-right: 8px;
                      cursor: pointer;
                    `}
                    onClick={addVariant}
                  >
                    add variant
                  </Button>
                </div>

                {currentElementVariant.nodeType === 'img' ? (
                  <>
                    <div
                      css={css`
                        margin-top: 10px;
                        display: flex;
                        flex-direction: column;
                      `}
                    >
                      <Text
                        variant="uppercase"
                        css={css`
                          color: #999;
                          margin-top: 15px;
                          margin-bottom: 5px;
                        `}
                      >
                        Image
                      </Text>
                      <InputGroup
                        label="src"
                        css={css`
                          margin-top: 0;
                          padding: 5px;
                          width: 100%;
                        `}
                      >
                        <Input
                          name="src"
                          value={currentElementVariant.src}
                          onChange={updateImageSrc}
                        />
                      </InputGroup>
                    </div>
                  </>
                ) : (
                  <>
                    <div
                      css={css`
                        margin-top: 10px;
                        display: flex;
                        flex-direction: column;
                      `}
                    >
                      <Text
                        variant="uppercase"
                        css={css`
                          width: 100%;
                          color: #999;
                          margin-top: 15px;
                          margin-bottom: 5px;
                        `}
                      >
                        Content
                      </Text>
                    </div>
                    <div
                      css={css`
                        display: flex;
                      `}
                    >
                      <HtmlEditor
                        name="content"
                        value={currentElementVariant.content}
                        onChange={updateContent}
                        onLoad={() => {}}
                        className="border border-solid border-1 border-gray-400"
                      ></HtmlEditor>
                    </div>
                  </>
                )}

                <div
                  css={css`
                    display: flex;
                    width: 406px;
                    flex-wrap: wrap;
                  `}
                >
                  <Text
                    variant="uppercase"
                    css={css`
                      width: 100%;
                      color: #999;
                      margin-top: 15px;
                      margin-bottom: 5px;
                    `}
                  >
                    Dimensions
                  </Text>
                  <InputGroup
                    label="width"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="width"
                      value={currentElementVariant.style.width}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>

                  <InputGroup
                    label="height"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      name="height"
                      value={currentElementVariant.style.height}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>
                  <Text
                    variant="uppercase"
                    css={css`
                      width: 100%;
                      color: #999;
                      margin-top: 15px;
                      margin-bottom: 5px;
                    `}
                  >
                    Locations
                  </Text>
                  <InputGroup
                    label="top"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      name="top"
                      value={currentElementVariant.style.top}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>

                  <InputGroup
                    label="bottom"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      name="bottom"
                      value={currentElementVariant.style.bottom}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>

                  <InputGroup
                    label="left"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      name="left"
                      value={currentElementVariant.style.left}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>
                  <InputGroup
                    label="right"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      name="right"
                      value={currentElementVariant.style.right}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>

                  <Text
                    variant="uppercase"
                    css={css`
                      width: 100%;
                      color: #999;
                      margin-top: 15px;
                      margin-bottom: 5px;
                    `}
                  >
                    Typography
                  </Text>

                  <InputGroup
                    label="font family"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="fontFamily"
                      value={currentElementVariant.style.fontFamily}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>

                  <InputGroup
                    label="font size"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="fontSize"
                      value={currentElementVariant.style.fontSize}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>

                  <InputGroup
                    label="font weight"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="fontWeight"
                      value={currentElementVariant.style.fontWeight}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>
                  <InputGroup
                    label="text align"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="textAlign"
                      value={currentElementVariant.style.textAlign}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>

                  <InputGroup
                    label="color"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="color"
                      value={currentElementVariant.style.color}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>
                  <InputGroup
                    label="font style"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="fontStyle"
                      value={currentElementVariant.style.fontStyle}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>

                  <Text
                    variant="uppercase"
                    css={css`
                      width: 100%;
                      color: #999;
                      margin-top: 15px;
                      margin-bottom: 5px;
                    `}
                  >
                    Background
                  </Text>
                  <InputGroup
                    label="background color"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="backgroundColor"
                      value={currentElementVariant.style.backgroundColor}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>
                  <InputGroup
                    label="background position"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="backgroundPosition"
                      value={currentElementVariant.style.backgroundPosition}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>
                  <InputGroup
                    label="background repeat"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="backgroundRepeat"
                      value={currentElementVariant.style.backgroundRepeat}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>
                  <InputGroup
                    label="background size"
                    css={css`
                      margin-top: 0;
                      padding: 5px;
                      width: 193px;
                    `}
                  >
                    <Input
                      type="text"
                      name="backgroundSize"
                      value={currentElementVariant.style.backgroundSize}
                      onChange={updateStyleProperty}
                    />
                  </InputGroup>
                </div>
              </div>
              <div
                css={css`
                  display: flex;
                  align-items: center;
                  justify-content: space-around;
                  height: 65px;
                  padding: 8px;
                  border-top: solid 1px #ececec;
                `}
              >
                <Button
                  type="button"
                  variant="outline"
                  intent="primary"
                  iconBefore={<IconEye />}
                  css={css`
                    width: 130px;
                  `}
                  onClick={applyStyle}
                >
                  Preview
                </Button>
                <Button
                  type="button"
                  variant="outline"
                  intent="primary"
                  loading={isSavingElement}
                  disabled={isSavingElement}
                  iconBefore={<IconSave />}
                  css={css`
                    width: 130px;
                  `}
                  onClick={saveElement}
                >
                  Save
                </Button>
                <Button
                  type="button"
                  variant="outline"
                  intent="primary"
                  disabled={!currentElementVariant.id || isSavingElement}
                  iconBefore={<IconTrash2 />}
                  css={css`
                    width: 130px;
                  `}
                  onClick={deleteElement}
                >
                  Delete
                </Button>
              </div>
            </div>

            {isLoadingElements && (
              <div
                css={css`
                  position: absolute;
                  top: 0;
                  left: 0;
                  bottom: 0;
                  right: 0;
                  backdrop-filter: blur(5px);
                  background-color: rgba(255, 255, 255, 0.5);
                  z-index: 10;
                `}
              >
                <div
                  css={css`
                    width: 300px;
                    height: 150px;
                    margin: auto;
                    top: 36%;
                    position: relative;
                  `}
                >
                  <Spinner label="Loading element" center />
                </div>
              </div>
            )}
          </div>
        </Draggable>
      )}

      <div
        css={css`
          position: fixed;
          display: flex;
          align-items: center;
          background: white;
          border-top: 1px solid #ccc;
          height: 50px;
          padding: 0 15px;
          bottom: 0;
          left: 0;
          right: 0;
          z-index: 99999999;
          pointer-events: all;
          box-shadow: 0 6px 3px 5px rgba(0, 0, 0, 0.29);
        `}
      >
        <Button
          type="button"
          variant="outline"
          intent="primary"
          disabled={isSavingElement || isDeletingElement}
          loading={isDeletingElement}
          iconBefore={<IconTrash2 />}
          css={css`
            width: 200px;
            margin-left: auto;
          `}
          onClick={deleteAllElements}
        >
          Clear all elements
        </Button>

        <Button
          type="button"
          variant="outline"
          intent="primary"
          disabled={isSavingElement || isDeletingElement}
          iconBefore={<IconX />}
          css={css`
            width: 160px;
            margin-left: 20px;
          `}
          onClick={() => window.close()}
        >
          Close editor
        </Button>
      </div>
      <ToastContainer />
      <style>{`
      .Toastify__toast-container {
        z-index: 9999;
        -webkit-transform: translate3d(0, 0, 9999px);
        position: fixed;
        padding: 4px;
        width: 320px;
        box-sizing: border-box;
        color: #fff;
      }
      .Toastify__toast-container--top-left {
        top: 1em;
        left: 1em;
      }
      .Toastify__toast-container--top-center {
        top: 1em;
        left: 50%;
        transform: translateX(-50%);
      }
      .Toastify__toast-container--top-right {
        top: 1em;
        right: 1em;
      }
      .Toastify__toast-container--bottom-left {
        bottom: 1em;
        left: 1em;
      }
      .Toastify__toast-container--bottom-center {
        bottom: 1em;
        left: 50%;
        transform: translateX(-50%);
      }
      .Toastify__toast-container--bottom-right {
        bottom: 1em;
        right: 1em;
      }

      @media only screen and (max-width : 480px) {
        .Toastify__toast-container {
          width: 100vw;
          padding: 0;
          left: 0;
          margin: 0;
        }
        .Toastify__toast-container--top-left, .Toastify__toast-container--top-center, .Toastify__toast-container--top-right {
          top: 0;
          transform: translateX(0);
        }
        .Toastify__toast-container--bottom-left, .Toastify__toast-container--bottom-center, .Toastify__toast-container--bottom-right {
          bottom: 0;
          transform: translateX(0);
        }
        .Toastify__toast-container--rtl {
          right: 0;
          left: initial;
        }
      }
      .Toastify__toast {
        position: relative;
        min-height: 64px;
        box-sizing: border-box;
        margin-bottom: 1rem;
        padding: 8px;
        border-radius: 1px;
        box-shadow: 0 1px 10px 0 rgba(0, 0, 0, 0.1), 0 2px 15px 0 rgba(0, 0, 0, 0.05);
        display: -ms-flexbox;
        display: flex;
        -ms-flex-pack: justify;
            justify-content: space-between;
        max-height: 800px;
        overflow: hidden;
        font-family: sans-serif;
        cursor: pointer;
        direction: ltr;
      }
      .Toastify__toast--rtl {
        direction: rtl;
      }
      .Toastify__toast--dark {
        background: #121212;
        color: #fff;
      }
      .Toastify__toast--default {
        background: #fff;
        color: #aaa;
      }
      .Toastify__toast--info {
        background: #3498db;
      }
      .Toastify__toast--success {
        background: #07bc0c;
      }
      .Toastify__toast--warning {
        background: #f1c40f;
      }
      .Toastify__toast--error {
        background: #e74c3c;
      }
      .Toastify__toast-body {
        margin: auto 0;
        -ms-flex: 1 1 auto;
            flex: 1 1 auto;
      }

      @media only screen and (max-width : 480px) {
        .Toastify__toast {
          margin-bottom: 0;
        }
      }
      .Toastify__close-button {
        color: #fff;
        background: transparent;
        outline: none;
        border: none;
        padding: 0;
        cursor: pointer;
        opacity: 0.7;
        transition: 0.3s ease;
        -ms-flex-item-align: start;
            align-self: flex-start;
      }
      .Toastify__close-button--default {
        color: #000;
        opacity: 0.3;
      }
      .Toastify__close-button > svg {
        fill: currentColor;
        height: 16px;
        width: 14px;
      }
      .Toastify__close-button:hover, .Toastify__close-button:focus {
        opacity: 1;
      }

      @keyframes Toastify__trackProgress {
        0% {
          transform: scaleX(1);
        }
        100% {
          transform: scaleX(0);
        }
      }
      .Toastify__progress-bar {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 5px;
        z-index: 9999;
        opacity: 0.7;
        background-color: rgba(255, 255, 255, 0.7);
        transform-origin: left;
      }
      .Toastify__progress-bar--animated {
        animation: Toastify__trackProgress linear 1 forwards;
      }
      .Toastify__progress-bar--controlled {
        transition: transform 0.2s;
      }
      .Toastify__progress-bar--rtl {
        right: 0;
        left: initial;
        transform-origin: right;
      }
      .Toastify__progress-bar--default {
        background: linear-gradient(to right, #4cd964, #5ac8fa, #007aff, #34aadc, #5856d6, #ff2d55);
      }
      .Toastify__progress-bar--dark {
        background: #bb86fc;
      }
      @keyframes Toastify__bounceInRight {
        from, 60%, 75%, 90%, to {
          animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
        }
        from {
          opacity: 0;
          transform: translate3d(3000px, 0, 0);
        }
        60% {
          opacity: 1;
          transform: translate3d(-25px, 0, 0);
        }
        75% {
          transform: translate3d(10px, 0, 0);
        }
        90% {
          transform: translate3d(-5px, 0, 0);
        }
        to {
          transform: none;
        }
      }
      @keyframes Toastify__bounceOutRight {
        20% {
          opacity: 1;
          transform: translate3d(-20px, 0, 0);
        }
        to {
          opacity: 0;
          transform: translate3d(2000px, 0, 0);
        }
      }
      @keyframes Toastify__bounceInLeft {
        from, 60%, 75%, 90%, to {
          animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
        }
        0% {
          opacity: 0;
          transform: translate3d(-3000px, 0, 0);
        }
        60% {
          opacity: 1;
          transform: translate3d(25px, 0, 0);
        }
        75% {
          transform: translate3d(-10px, 0, 0);
        }
        90% {
          transform: translate3d(5px, 0, 0);
        }
        to {
          transform: none;
        }
      }
      @keyframes Toastify__bounceOutLeft {
        20% {
          opacity: 1;
          transform: translate3d(20px, 0, 0);
        }
        to {
          opacity: 0;
          transform: translate3d(-2000px, 0, 0);
        }
      }
      @keyframes Toastify__bounceInUp {
        from, 60%, 75%, 90%, to {
          animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
        }
        from {
          opacity: 0;
          transform: translate3d(0, 3000px, 0);
        }
        60% {
          opacity: 1;
          transform: translate3d(0, -20px, 0);
        }
        75% {
          transform: translate3d(0, 10px, 0);
        }
        90% {
          transform: translate3d(0, -5px, 0);
        }
        to {
          transform: translate3d(0, 0, 0);
        }
      }
      @keyframes Toastify__bounceOutUp {
        20% {
          transform: translate3d(0, -10px, 0);
        }
        40%, 45% {
          opacity: 1;
          transform: translate3d(0, 20px, 0);
        }
        to {
          opacity: 0;
          transform: translate3d(0, -2000px, 0);
        }
      }
      @keyframes Toastify__bounceInDown {
        from, 60%, 75%, 90%, to {
          animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
        }
        0% {
          opacity: 0;
          transform: translate3d(0, -3000px, 0);
        }
        60% {
          opacity: 1;
          transform: translate3d(0, 25px, 0);
        }
        75% {
          transform: translate3d(0, -10px, 0);
        }
        90% {
          transform: translate3d(0, 5px, 0);
        }
        to {
          transform: none;
        }
      }
      @keyframes Toastify__bounceOutDown {
        20% {
          transform: translate3d(0, 10px, 0);
        }
        40%, 45% {
          opacity: 1;
          transform: translate3d(0, -20px, 0);
        }
        to {
          opacity: 0;
          transform: translate3d(0, 2000px, 0);
        }
      }
      .Toastify__bounce-enter--top-left, .Toastify__bounce-enter--bottom-left {
        animation-name: Toastify__bounceInLeft;
      }
      .Toastify__bounce-enter--top-right, .Toastify__bounce-enter--bottom-right {
        animation-name: Toastify__bounceInRight;
      }
      .Toastify__bounce-enter--top-center {
        animation-name: Toastify__bounceInDown;
      }
      .Toastify__bounce-enter--bottom-center {
        animation-name: Toastify__bounceInUp;
      }

      .Toastify__bounce-exit--top-left, .Toastify__bounce-exit--bottom-left {
        animation-name: Toastify__bounceOutLeft;
      }
      .Toastify__bounce-exit--top-right, .Toastify__bounce-exit--bottom-right {
        animation-name: Toastify__bounceOutRight;
      }
      .Toastify__bounce-exit--top-center {
        animation-name: Toastify__bounceOutUp;
      }
      .Toastify__bounce-exit--bottom-center {
        animation-name: Toastify__bounceOutDown;
      }

      @keyframes Toastify__zoomIn {
        from {
          opacity: 0;
          transform: scale3d(0.3, 0.3, 0.3);
        }
        50% {
          opacity: 1;
        }
      }
      @keyframes Toastify__zoomOut {
        from {
          opacity: 1;
        }
        50% {
          opacity: 0;
          transform: scale3d(0.3, 0.3, 0.3);
        }
        to {
          opacity: 0;
        }
      }
      .Toastify__zoom-enter {
        animation-name: Toastify__zoomIn;
      }

      .Toastify__zoom-exit {
        animation-name: Toastify__zoomOut;
      }

      @keyframes Toastify__flipIn {
        from {
          transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
          animation-timing-function: ease-in;
          opacity: 0;
        }
        40% {
          transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
          animation-timing-function: ease-in;
        }
        60% {
          transform: perspective(400px) rotate3d(1, 0, 0, 10deg);
          opacity: 1;
        }
        80% {
          transform: perspective(400px) rotate3d(1, 0, 0, -5deg);
        }
        to {
          transform: perspective(400px);
        }
      }
      @keyframes Toastify__flipOut {
        from {
          transform: perspective(400px);
        }
        30% {
          transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
          opacity: 1;
        }
        to {
          transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
          opacity: 0;
        }
      }
      .Toastify__flip-enter {
        animation-name: Toastify__flipIn;
      }

      .Toastify__flip-exit {
        animation-name: Toastify__flipOut;
      }

      @keyframes Toastify__slideInRight {
        from {
          transform: translate3d(110%, 0, 0);
          visibility: visible;
        }
        to {
          transform: translate3d(0, 0, 0);
        }
      }
      @keyframes Toastify__slideInLeft {
        from {
          transform: translate3d(-110%, 0, 0);
          visibility: visible;
        }
        to {
          transform: translate3d(0, 0, 0);
        }
      }
      @keyframes Toastify__slideInUp {
        from {
          transform: translate3d(0, 110%, 0);
          visibility: visible;
        }
        to {
          transform: translate3d(0, 0, 0);
        }
      }
      @keyframes Toastify__slideInDown {
        from {
          transform: translate3d(0, -110%, 0);
          visibility: visible;
        }
        to {
          transform: translate3d(0, 0, 0);
        }
      }
      @keyframes Toastify__slideOutRight {
        from {
          transform: translate3d(0, 0, 0);
        }
        to {
          visibility: hidden;
          transform: translate3d(110%, 0, 0);
        }
      }
      @keyframes Toastify__slideOutLeft {
        from {
          transform: translate3d(0, 0, 0);
        }
        to {
          visibility: hidden;
          transform: translate3d(-110%, 0, 0);
        }
      }
      @keyframes Toastify__slideOutDown {
        from {
          transform: translate3d(0, 0, 0);
        }
        to {
          visibility: hidden;
          transform: translate3d(0, 500px, 0);
        }
      }
      @keyframes Toastify__slideOutUp {
        from {
          transform: translate3d(0, 0, 0);
        }
        to {
          visibility: hidden;
          transform: translate3d(0, -500px, 0);
        }
      }
      .Toastify__slide-enter--top-left, .Toastify__slide-enter--bottom-left {
        animation-name: Toastify__slideInLeft;
      }
      .Toastify__slide-enter--top-right, .Toastify__slide-enter--bottom-right {
        animation-name: Toastify__slideInRight;
      }
      .Toastify__slide-enter--top-center {
        animation-name: Toastify__slideInDown;
      }
      .Toastify__slide-enter--bottom-center {
        animation-name: Toastify__slideInUp;
      }

      .Toastify__slide-exit--top-left, .Toastify__slide-exit--bottom-left {
        animation-name: Toastify__slideOutLeft;
      }
      .Toastify__slide-exit--top-right, .Toastify__slide-exit--bottom-right {
        animation-name: Toastify__slideOutRight;
      }
      .Toastify__slide-exit--top-center {
        animation-name: Toastify__slideOutUp;
      }
      .Toastify__slide-exit--bottom-center {
        animation-name: Toastify__slideOutDown;
      }

      `}</style>
    </>
  );
};

export default App;
