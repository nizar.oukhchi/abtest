const applyStyleOnDomElement = (node, element) => {
  
  if (node && element) {
    if (element.isMultiple) {
      const nodes = document.querySelectorAll(element.selector);
      if (nodes && nodes.length > 0) {
        nodes.forEach((item) => {
          item.innerHTML = element.content;
          item.src = element.src;
          if (element.style && Object.keys(element.style).length) {
            for (const key in element.style) {
              if (element.style.hasOwnProperty(key)) {
                item.style[key] = element.style[key];
              }
            }
          }
        });
      }
    } else {
      node.innerHTML = element.content;
      node.src = element.src;

      if (element.style && Object.keys(element.style).length) {
        for (const key in element.style) {
          if (element.style.hasOwnProperty(key)) {
            node.style[key] = element.style[key];
          }
        }
      }
    }
  }
};

export default applyStyleOnDomElement;