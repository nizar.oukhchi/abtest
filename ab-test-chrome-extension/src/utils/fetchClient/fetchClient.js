import axios from 'axios';
import { BASE_URL } from '../constants';

const fetchClient = () => {
  const defaultOptions = {
    baseURL: BASE_URL,
    method: 'get',
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const instance = axios.create(defaultOptions);

  instance.interceptors.request.use(
    config => {
      const token = 'getToken()';
      config.headers.Authorization = token ? `Bearer ${token}` : '';

      return { ...config };
    },
    error => {
      return Promise.reject(error);
    }
  );

  return instance;
};

export default fetchClient();
