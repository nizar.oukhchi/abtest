import getCssSelector from 'css-selector-generator';

const findSelector = (element, isMultiple) => {
  const className = element.className;
  return isMultiple && className
    ? className
        .split(' ')
        .reduce((accumulator, item) => accumulator + '.' + item, '')
    : getCssSelector(element);
};

export default findSelector;
