const isDescendant = child => {
  const parent = document.querySelector("#extension");

  var node = child.parentNode;

  while (node != null) {
    if (node == parent) {
      return true;
    }
    node = node.parentNode;
  }
  return false;
};

export default isDescendant;
