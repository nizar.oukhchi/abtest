function hasClass(element, selector) {
  if (typeof element.className === 'object') return true;
  if (
    element.className &&
    typeof element.className === 'string' &&
    element.className.split(' ').indexOf(selector) >= 0
  )
    return true;
  return element.parentNode && hasClass(element.parentNode, selector);
}

export default hasClass;
