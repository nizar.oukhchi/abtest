import React from 'react';
import { css } from '@emotion/core';
import { Text } from 'sancho';

const Highlighter = ({ position, name, className, color, zIndex }) => {
  return name ? (
    <div
      className={className}
      css={css`
        position: absolute;
        display: hidden;
        cursor: pointer;
        border: solid 2px ${color};
        pointer-events: none;
        z-index: ${zIndex};
      `}
      style={position}
    >
      <div
        css={css`
          position: absolute;
          background-color: ${color};

          top: -26px;
          left: -2px;
        `}
      >
        <Text
          css={css`
            color: white;
            font-weight: bold;
          `}
        >
          {`<${name}/>`}
        </Text>
      </div>
    </div>
  ) : null;
};

export default Highlighter;
