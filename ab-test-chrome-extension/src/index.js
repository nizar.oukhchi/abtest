import React from 'react';
import ReactDOM from 'react-dom';
import { Global, css, CacheProvider } from '@emotion/core';
import createCache from '@emotion/cache';

import App from './App';

const el = document.createElement('div');
el.setAttribute('id', 'extension-wrapper');
document.body.appendChild(el);
const shadowTarget = el.attachShadow({ mode: 'closed' });
shadowTarget.innerHTML = `<div id="extension"></div>`;

const cache = createCache({
  container: shadowTarget,
});

const extension = shadowTarget.getElementById('extension');

ReactDOM.render(
  <CacheProvider value={cache}>
    <>
      <Global
        styles={css`
          :host {
            pointer-events: none;
            z-index: 999999;
            direction: ltr !important;
            text-align: left;
          }

          #extension .extension-html-editor {
            display: flex;
            width: 100%;
            overflow: auto;
            box-shadow: 0 0 0 2px transparent inset,
              0 0 0 1px hsla(210, 10.8%, 14.5%, 0.2) inset;
            border-radius: 0.25rem;
            -webkit-transition: background 0.25s cubic-bezier(0.35, 0, 0.25, 1),
              border-color 0.15s cubic-bezier(0.35, 0, 0.25, 1),
              box-shadow 0.15s cubic-bezier(0.35, 0, 0.25, 1);
            transition: background 0.25s cubic-bezier(0.35, 0, 0.25, 1),
              border-color 0.15s cubic-bezier(0.35, 0, 0.25, 1),
              box-shadow 0.15s cubic-bezier(0.35, 0, 0.25, 1);
            padding: 8px;
          }

          #extension .extension-html-editor .react-codemirror2 {
            font-size: 20px;
          }

          #extension .extension-html-editor .react-codemirror2 > .CodeMirror {
            height: 150px;
          }
        `}
      />
      <App />
    </>
  </CacheProvider>,
  extension
);

/* Object.defineProperty(extension, "ownerDocument", { value: shadowTarget });
shadowTarget.createElement = (...args) => document.createElement(...args); */

Object.defineProperty(extension, 'ownerDocument', { value: shadowTarget });
shadowTarget.createElement = (...args) => document.createElement(...args);
shadowTarget.createElementNS = (...args) => document.createElementNS(...args);
shadowTarget.createTextNode = (...args) => document.createTextNode(...args);
