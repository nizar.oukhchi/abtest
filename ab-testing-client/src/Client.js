import store from 'store';
import axios from 'axios';

import config from './config';
import { applyDomElements, handleConversionEvents } from './helpers';

const isNotIE = !/Trident|MSIE/.test(navigator.userAgent);

export default class AlgorithmClient {
  constructor(apiKey) {
    this.apiKey = apiKey;
    this.webSiteUrl = isNotIE && new URL(window.location.href);
    this.url = `${config.API_URL}/api/users/sync?url=${this.webSiteUrl}`;
    this.conversionUrl = `${config.API_URL}/api/users/converted`;
    this.currentClientId = store.get('CURRENT_USER_ID');
    this.currentVariantId = store.get('ALGORITHM_CURRENT_VARIANT_ID');
  }

  onConversion() {
    axios.get(this.conversionUrl, {
      headers: {
        'content-type': 'application/json',
        'User-Id': this.currentClientId || '',
      },
    });
  }

  init() {
    if (isNotIE) {
      const url = new URL(document.location.href);
      const previewVariant = url.searchParams.get('bandit-preview');
      if (previewVariant) {
        axios
          .get(`${config.API_URL}/api/thompsons/preview/${previewVariant}`, {
            headers: {
              'content-type': 'application/json',
            },
          })
          .then((response) => {
            const { domElements } = response.data;
            applyDomElements(domElements);
          });
      } else {
        const conversionPixels = JSON.parse(store.get('CONVERSION_PIXELS'));
        conversionPixels.forEach((element) => {
          if (element.content.length > 0) {
            if (
              element.test === 'equals' &&
              element.content === window.location.href
            ) {
              this.onConversion();
            }

            if (
              element.test === 'contains' &&
              window.location.href.includes(element.content)
            ) {
              this.onConversion();
            }
          }
        });
        axios
          .get(this.url, {
            headers: {
              'content-type': 'application/json',
              'User-Id': this.currentClientId || '',
              'Client-Key': this.apiKey || '',
            },
          })
          .then((response) => {
            const client = response.data;
            const {
              id,
              conversionSelectors,
              variant,
              conversionPixels,
            } = client;

            if (conversionPixels && conversionPixels.length > 0) {
              store.set('CONVERSION_PIXELS', JSON.stringify(conversionPixels));
            }

            if (!this.currentClientId) {
              store.set('CURRENT_USER_ID', id);
              this.currentClientId = id;
            }

            if (this.currentVariantId !== variant.id) {
              this.currentVariantId = variant.id;
              store.set('ALGORITHM_CURRENT_VARIANT_ID', variant.id);
            }

            const { domElements } = variant;
            applyDomElements(domElements);
            const interval = setInterval(() => {
              clearInterval(interval);
              handleConversionEvents(
                'click',
                conversionSelectors.clickEventElementSelector,
                () => {
                  this.onConversion();
                }
              );
              handleConversionEvents(
                'submit',
                conversionSelectors.submitEventElementSelector,
                () => {
                  this.onConversion();
                }
              );
            }, 300);
          })
          .catch((err) => {
            console.error(err);
          });
      }
    }
  }
}
