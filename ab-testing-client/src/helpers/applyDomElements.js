const applyDomElements = (domElements) => {
  domElements.forEach((element) => {
    if (element.isMultiple) {
      const interval = setInterval(() => {
        const nodes = document.querySelectorAll(element.selector);
        if (nodes && nodes.length > 0) {
          clearInterval(interval);
          nodes.forEach((node) => {
            node.innerHTML = element.content;
            node.src = element.src;
            if (element.style && Object.keys(element.style).length) {
              for (const key in element.style) {
                if (element.style.hasOwnProperty(key)) {
                  node.style[key] = element.style[key];
                }
              }
            }
          });
        }
      }, 300);
    } else {
      const interval = setInterval(() => {
        const node = document.querySelector(element.selector);
        if (node) {
          clearInterval(interval);
          node.innerHTML = element.content;
          node.src = element.src;
          if (element.style && Object.keys(element.style).length) {
            for (const key in element.style) {
              if (element.style.hasOwnProperty(key)) {
                node.style[key] = element.style[key];
              }
            }
          }
        }
      }, 300);
    }
  });
};

export default applyDomElements;
